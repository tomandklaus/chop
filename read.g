#############################################################################
##
##  read.g                chop package                      Max Neunhoeffer
##                                                             Felix Noeske
##
##  Copyright 2005 Lehrstuhl D f�r Mathematik, RWTH Aachen
##
##  Reading the implementation part of the chop package.
##
#############################################################################

ReadPackage("chop","gap/chain.gi");
ReadPackage("chop","gap/tree.gi");
ReadPackage("chop","gap/wordgens.gi");
ReadPackage("chop","gap/chop.gi");

