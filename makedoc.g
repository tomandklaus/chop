##  this creates the documentation, needs: GAPDoc package, latex, pdflatex,
##  mkindex, dvips
##  
##  Call this with GAP.
##

RequirePackage("GAPDoc");

Read( "doc/ListOfDocFiles.g" );

MakeGAPDocDoc("doc", "chop", list, "chop");

GAPDocManualLab("chop");

quit;

