#############################################################################
##
##  init.g                chop package                      Max Neunhoeffer
##                                                             Felix Noeske
##
##  Copyright 2005 Lehrstuhl D f�r Mathematik, RWTH Aachen
##
##  Reading the declaration part of the chop package.
##
#############################################################################

ReadPackage("chop","gap/chain.gd");
ReadPackage("chop","gap/tree.gd");
ReadPackage("chop","gap/wordgens.gd");
ReadPackage("chop","gap/chop.gd");

