AtlasRepChopTester := function( groupname, repnr, slpnr )
  local cdegs,cdims,cgens,comps,degs,dims,gens,i,lmp,m,mm,r,s,ti,ti2,ti3,ti4,x;
  gens := AtlasGenerators(groupname,repnr).generators;
  if IsPerm(gens[1]) then
      lmp := LargestMovedPoint(gens);
      gens := List(gens,x->PermutationMat(x,lmp,GF(2)));
      for x in gens do
          ConvertToMatrixRep(x,2);
      od;
  fi;
  if slpnr <> 0 then
      s := AtlasStraightLineProgram(groupname,slpnr).program;
      gens := ResultOfStraightLineProgram(s,gens);
  fi;
  cgens := List(gens,CMat);
  m := GModuleByMats(gens,BaseDomain(cgens[1]));
  mm := Module(cgens);

  GASMAN("collect");
  ti := Runtime();
  comps := MTX.CollectedFactors(m);
  ti2 := Runtime();
  GASMAN("collect");
  ti3 := Runtime();
  r := Chop( mm );
  ti4 := Runtime();
  dims := [];
  for i in comps do
      Append(dims,ListWithIdenticalEntries(i[2],i[1].dimension));
  od;
  dims := Collected(dims);
  cdims := Collected(List(r.db{r.acs},Dimension));
  if dims <> cdims then
      Error("Dimensions of simples are not equal!");
  fi;
  degs := [];
  for i in comps do
      Append(degs,ListWithIdenticalEntries(i[2],MTX.DegreeSplittingField(i[1])/
                                DegreeOverPrimeField(BaseDomain(cgens[1]))));
  od;
  degs := Collected(degs);
  cdegs := Collected(List(r.db{r.acs},DegreeOfSplittingField));
  if degs <> cdegs then
      Error("Degrees of splitting fields are not equal!");
  fi;
  Print("G=",groupname," rep=",repnr," dim=",Length(gens[1]),
        " fieldsize=",Size(BaseDomain(cgens[1]))," slpnr=",slpnr,
        " time SMTX: ",ti2-ti," time Chop: ",ti4-ti3,"\n");
end;

AtlasRepSporadicsTester := function()
  local i,j,k,maxs,names,reps;
  names := ["M11", "M12" ];
  reps := [[1..33],[1..35]];
  maxs := [[0..5],[0..11]];
  for i in [1..Length(names)] do
      for j in reps[i] do
          for k in maxs[i] do
              AtlasRepChopTester(names[i],j,k);
          od;
      od;
  od;
end;

