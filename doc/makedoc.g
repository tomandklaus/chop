##  this creates the documentation, needs: GAPDoc package, latex, pdflatex,
##  mkindex, dvips
##  
##  Call this with GAP.
##

LoadPackage( "GAPDoc" );

# Loading the basic package will load all of the necessary other packages
#   for proper linking in the documentation.
LoadPackage( "chop" );

SetGapDocLaTeXOptions( "utf8" );

bib := ParseBibFiles( "chop.bib" );
WriteBibXMLextFile( "chopBib.xml", bib );

Read( "ListOfDocFiles.g" );

PrintTo( "VERSION", PackageInfo( "chop" )[1].Version );

MakeGAPDocDoc( DirectoryCurrent(), "chop", list, "chop" );

GAPDocManualLab( "chop" );

QUIT;
