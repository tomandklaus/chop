#############################################################################
##
##  tree.gi              chop package                        Max Neunhoeffer
##                                                              Felix Noeske
##
##  Copyright 2005 Lehrstuhl D für Mathematik, RWTH Aachen
##
##  Implementation stuff for binary trees
##
#############################################################################

InstallValue( BinaryTreeType, 
    NewType( BinaryTreesFamily, IsBinaryTree and IsAttributeStoringRep ) );

InstallMethod( BinaryTree, "constructor method", [IsObject],
  function(d)
    local r;
    r := rec( data := d );
    Objectify( BinaryTreeType, r);
    return r;
  end );

InstallMethod( BinaryTree, "constructor method", [IsObject, IsObject, IsObject],
  function(d,l,r)
    local rr;
    rr := rec( data := d );
    Objectify( BinaryTreeType, rr);
    if l <> fail then SetLeft(rr,l); fi;
    if r <> fail then SetRight(rr,r); fi;
    return rr;
  end );

InstallMethod( Data, "access of node data", [IsBinaryTree],
  function( t )
    return t!.data;
  end );

InstallMethod( Data, "for fail", [IsBool], ReturnFail );
InstallMethod( Left, "for fail", [IsBool], ReturnFail );
InstallMethod( Right, "for fail", [IsBool], ReturnFail );

InstallMethod( ViewObj, "for binary trees", [IsBinaryTree],
  function(t)
    Print("<binary tree data=",t!.data);
    if HasLeft(t) then
        Print(" with left");
        if HasRight(t) then 
            Print(" and right subtrees>");
        else
            Print(" subtree>");
        fi;
    elif HasRight(t) then
        Print(" with right subtree>");
    else
        Print(" leaf>");
    fi;
  end );

InstallMethod( PrintObj, "for binarytrees", [IsBinaryTree],
  function(t)
    Print( "BinaryTree(",Data(t),"," );
    if HasLeft(t) then
        PrintObj( Left(t) );
    else
        Print( "fail" );
    fi;
    Print(",");
    if HasRight(t) then
        PrintObj( Right(t) );
    else
        Print( "fail" );
    fi;
    Print(")\n");
  end );

SHOWTREE_INDENT := 0;
InstallMethod( ShowTree, "for a binary tree", [IsBinaryTree],
  function(t)
    ViewObj(t); Print("\n");
    if HasLeft(t) then
        Print(String("",SHOWTREE_INDENT),"L:");
        SHOWTREE_INDENT := SHOWTREE_INDENT + 2;
        ShowTree(Left(t));
        SHOWTREE_INDENT := SHOWTREE_INDENT - 2;
    fi;
    if HasRight(t) then
        Print(String("",SHOWTREE_INDENT),"R:");
        SHOWTREE_INDENT := SHOWTREE_INDENT + 2;
        ShowTree(Right(t));
        SHOWTREE_INDENT := SHOWTREE_INDENT - 2;
    fi;
  end );

InstallMethod( \=, "for two binary trees", [IsBinaryTree, IsBinaryTree],
  function( t1, t2 )
    if t1!.data <> t2!.data then return false; fi;
    # Check left hand side:
    if (HasLeft(t1) <> HasLeft(t2)) or
       (HasRight(t1) <> HasRight(t2)) then 
        return false; 
    fi;
    if HasLeft(t1) and Left(t1) <> Left(t2) then return false; fi;
    if HasRight(t1) and Right(t1) <> Right(t2) then return false; fi;
    return true;
  end );

InstallMethod( IO_Pickle, "for a binary tree object",
  [ IsFile, IsBinaryTree ],
  function( f, tr )
    return IO_GenericObjectPickler(f,"TREE",[],tr,[Left,Right],[],["data"]);
  end);

IO_Unpicklers.TREE :=
  function(f)
    local tr;
    tr := rec();
    Objectify(BinaryTreeType,tr);
    return IO_GenericObjectUnpickler(f,tr,[Left,Right],[]);
  end;
    

