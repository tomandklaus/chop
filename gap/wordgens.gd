#############################################################################
##
##  wordgens.gd              chop package     
##                                                           Max Neunhoeffer
##                                                              Felix Noeske
##
##  Copyright 2006 Lehrstuhl D für Mathematik, RWTH Aachen
##
##  Declaration stuff for word generators.
##
#############################################################################


#############################################################################
# Some typing things:
#############################################################################

BindGlobal( "WordGeneratorsFamily", NewFamily( "WordGeneratorsFamily" ) );
DeclareCategory( "IsWordGenerator", IsComponentObjectRep );
DeclareCategory( "IsWordGeneratorGAP", IsWordGenerator );

DeclareOperation( "WordGenerator", [ IsList, IsOperation, IsRecord ] );
DeclareOperation( "WordGenerator", [ IsList, IsOperation ] );
# Possible options:
#   field: may be a subfield of the BaseDomain of the generator
#   randomsource: an optional random source, otherwise, the global one is taken
DeclareOperation( "WordGeneratorInit", [ IsWordGenerator, IsRecord ] );
# This is called internally with a component object that has only the
# component "generators" bound, and with a (possibly empty) options record.

DeclareOperation( "NextWord", [ IsWordGenerator ] );
DeclareOperation( "EvaluateWord", [ IsWordGenerator ] );
DeclareOperation( "CurrentWord", [ IsWordGenerator ] );
DeclareOperation( "SetWord", [ IsWordGenerator, IsObject ] );
DeclareOperation( "ClearCache", [ IsWordGenerator ] );
DeclareOperation( "Memory", [ IsWordGenerator ] );
# This does not include the generators but only the additional matrices
# in the word cache!
DeclareOperation( "TypeOfWordGenerator", [ IsWordGenerator ] );
# Returns an operation that can be used to create a new word generator
# of the same type.

DeclareOperation( "TransposedWord", [ IsObject ] );

