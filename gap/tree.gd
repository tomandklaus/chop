#############################################################################
##
##  tree.gd              chop package                        Max Neunhoeffer
##                                                              Felix Noeske
##
##  Copyright 2005 Lehrstuhl D für Mathematik, RWTH Aachen
##
##  Declaration stuff for binary trees
##
#############################################################################

DeclareCategory( "IsBinaryTree", IsComponentObjectRep and 
                                 IsAttributeStoringRep );

DeclareGlobalVariable( "BinaryTreeType" );

BindGlobal( "BinaryTreesFamily", NewFamily("BinaryTreesFamily",IsBinaryTree ));

DeclareAttribute( "Left", IsBinaryTree, "mutable" );
DeclareAttribute( "Right", IsBinaryTree, "mutable" );
DeclareOperation( "Data", [IsObject] );
DeclareAttribute( "Left", IsBool, "mutable" );
DeclareAttribute( "Right", IsBool, "mutable" );

DeclareOperation( "BinaryTree", [IsObject, IsObject, IsObject] );
DeclareOperation( "BinaryTree", [IsObject] );

DeclareOperation( "ShowTree", [IsObject] );

