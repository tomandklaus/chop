#############################################################################
##
##  chain.gd              chop package                        
##                                                           Max Neunhoeffer
##                                                              Felix Noeske
##
##  Copyright 2005 Lehrstuhl D für Mathematik, RWTH Aachen
##
##  Declaration stuff for the chainworker
##
#############################################################################

BindGlobal( "SubgroupChainsFamily", NewFamily( "SubgroupChainsFamily" ) );
DeclareCategory( "IsSubgroupChain", IsComponentObjectRep );
DeclareRepresentation( "IsStdSubgroupChainRep", IsSubgroupChain,
  [ "slp",    # an slp expressing the generators of the next subgroup in terms
              # of the current one or false, if the next subgroup is trivial
    "r",      # false if the next subgroup in the chain is trivial, otherwise
              # another instance of a chain describing the subchain
    "sgen",   # a Schreier vector for a transversal, 
              # here the generators applied are stored
    "spos",   # the same, here the positions to which the generators are
              # applied are stored
    "size",   # Size of the current subgroup
    "height", # number of subgroups to follow except trivial subgroup
  ] );
BindGlobal( "StdSubgroupChainType",
    NewType(SubgroupChainsFamily,IsSubgroupChain and IsStdSubgroupChainRep ) );

DeclareGlobalFunction( "SubgroupChain" );
DeclareGlobalFunction( "PrepareChain" );
DeclareGlobalFunction( "PrepareCompositionSeriesChain" );
DeclareGlobalFunction( "SubgroupChainBySLPs" );
DeclareOperation( "ChainWorker", [IsList,IsSubgroupChain,IsFunction,IsObject] );
DeclareOperation( "ChainWorker2",
  [ IsList, IsList, IsSubgroupChain, IsFunction, IsObject ] );
DeclareGlobalFunction( "DoworkMaschke" );
DeclareGlobalFunction( "DoworkMaschke2" );
DeclareOperation( "CalcChain", [ IsList, IsSubgroupChain, IsList ] );


