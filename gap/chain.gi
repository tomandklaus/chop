#############################################################################
##
##  chain.gi              chop package                        
##                                                           Max Neunhoeffer
##                                                              Felix Noeske
##
##  Copyright 2005 Lehrstuhl D für Mathematik, RWTH Aachen
##
##  Implementation stuff for the chainworker
##
#############################################################################

InstallMethod( ViewObj, "for a std subgroup chain",
  [ IsSubgroupChain and IsStdSubgroupChainRep ],
  function( r )
    local l;
    Print( "<subgroup chain sizes=[" );
    l := r;
    while l!.r <> false do
        Print( l!.size,"," );
        l := l!.r;
    od;
    Print( l!.size,",1]>" );
  end );

InstallMethod( PrintObj, "for a std subgroup chain",
  [ IsSubgroupChain and IsStdSubgroupChainRep ],
  function( r )
    Print( "SubgroupChain(",r!.size,",",r!.height,",",r!.slp,",",r!.sgen,",",
           r!.spos,"," );
    if r!.r = false then
      Print( "false)" );
    else
      Print( "\n ",r!.r,")" );
    fi;
  end );

InstallGlobalFunction( SubgroupChain,
function( size, height, slp, sgen, spos, r )
  local ch;
  ch := rec( size := size, height := height, slp := slp, sgen := sgen,
             spos := spos, r := r );
  Objectify(StdSubgroupChainType,ch);
  return ch;
end );

InstallGlobalFunction( PrepareChain,
function(l)
  # l is a descending chain of groups in some nice representation
  # (possibly ending in the trivial subgroup)
  # (i.e. we have to be able to use "FactorCosetAction", basically this
  # means we need permutation groups)
  local G,L,P,U,ac,i,ind,j,o,os,pgens,q,r,s;
  L := Length(l);
  G := l[1];
  if L = 1 then
    r := rec(r := false, slp := false, height := 0);
    U := TrivialSubgroup(G);
  elif L = 2 and IsTrivial(l[2]) then
    r := rec(r := false, slp := false, height := 0);
    U := l[2];
  else
    U := l[2];
    r := rec(slp := FindShortGeneratorsOfSubgroup(G,U,\in).slp);
    U := GroupWithGenerators(ResultOfStraightLineProgram(r.slp,
                                                         GeneratorsOfGroup(G)));
    r.r := PrepareChain(Concatenation([U],l{[3..L]}));
    r.height := r.r!.height+1;
  fi;
  r.size := Size(G);
  # Now we have to calculate a Schreier vector to have a transversal:
  ac := FactorCosetAction(G,U);
  P := Image(ac);
  pgens := GeneratorsOfGroup(P);
  o := Orb(pgens,1,OnPoints,0,rec( schreier := true ) );
  Enumerate(o);
  r.sgen := o!.schreiergen;
  r.spos := o!.schreierpos;
  Objectify(StdSubgroupChainType, r);
  return r;
end );

InstallGlobalFunction( PrepareCompositionSeriesChain,
function(pg)
  local l;
  l := ShallowCopy(CompositionSeries(pg));
  l[1] := pg;
  return PrepareChain(l);
end );

InstallGlobalFunction( SubgroupChainBySLPs,
function( slpl, sizes )
  local r,r2;
  if Length(slpl) = 1 then
      r := rec( r := false, slp := false, height := 0, size := sizes[2],
                spos := [], sgen := [] );
      Objectify(StdSubgroupChainType,r);
  else
      r := SubgroupChainBySLPs(slpl{[2..Length(slpl)]},
                               sizes{[2..Length(sizes)]});
  fi;
  r2 := rec( slp := slpl[1], r := r, height := r!.height+1, size := sizes[1],
             sgen := [], spos := [] );
  Objectify(StdSubgroupChainType,r2);
  return r2; 
end );

InstallOtherMethod( Size, "for a std subgroup chain",
  [ IsSubgroupChain and IsStdSubgroupChainRep ],
  function( r )
    return r!.size;
  end );

InstallMethod( ChainWorker, 
  "for a list of generators, a subgroup chain, a function, and a seed",
  [ IsList, IsSubgroupChain and IsStdSubgroupChainRep, IsFunction, IsObject ],
function(G,r,dowork,seed)
  # G and U lists of generators
  local U,i,o,res,subres,t;
  if r!.r = false then
    subres := seed;
  else
    U := ResultOfStraightLineProgram(r!.slp,G);
    subres := ChainWorker(U,r!.r,dowork,seed);
  fi;
  o := [One(G[1])];
  res := dowork(One(G[1]),fail,subres);
  for i in [2..Length(r!.sgen)] do
    Info(InfoChop,3,"Chainworker: ",r!.height,": ",i," (",Length(r!.sgen),")");
    t := o[r!.spos[i]] * G[r!.sgen[i]];
    Add(o,t);
    res := dowork(t,res,subres);
  od;
  return res;
end );

InstallMethod( ChainWorker2,
  "for two lists of generators, a subgroup chain, a function, and a seed",
  [ IsList, IsList, IsSubgroupChain and IsStdSubgroupChainRep, IsFunction,
    IsObject ],
function(G,GG,r,dowork,seed)
  # G, GG, U and UU lists of generators
  local U,UU,i,o,oo,res,subres,t,tt;
  if r!.r = false then
    subres := seed;
  else
    U := ResultOfStraightLineProgram(r!.slp,G);
    UU := ResultOfStraightLineProgram(r!.slp,GG);
    subres := ChainWorker2(U,UU,r!.r,dowork,seed);
  fi;
  o := [One(G[1])];
  oo := [One(GG[1])];
  res := dowork(One(G[1]),One(GG[1]),fail,subres);
  for i in [2..Length(r!.sgen)] do
    Info(InfoChop,3,"Chainworker: ",r!.height,": ",i," (",Length(r!.sgen),")");
    t := o[r!.spos[i]] * G[r!.sgen[i]];
    Add(o,t);
    tt := oo[r!.spos[i]] * GG[r!.sgen[i]];
    Add(oo,tt);
    res := dowork(t,tt,res,subres);
  od;
  return res;
end );

InstallGlobalFunction( DoworkMaschke,
function(t,res,subres)
  if res = fail then
    return subres;
  else
    return res + t^-1 * subres * t;
  fi;
end );

InstallGlobalFunction( DoworkMaschke2,
function(t,tt,res,subres)
  if res = fail then
    return subres;
  else
    return res + t^-1 * subres * tt;
  fi;
end );

InstallMethod( CalcChain, 
  "for a list of generators, a subgroup chain, and a mutable list",
  [ IsList, IsSubgroupChain and IsStdSubgroupChainRep, IsMutable and IsList ],
function(gens,r,l)
  if r!.slp <> false then
      CalcChain(ResultOfStraightLineProgram(r!.slp,gens),r!.r,l);
  fi;
  Add(l,gens,1);
  return l;
end );

InstallMethod( IO_Pickle, "for a subgroup chain object",
  [ IsFile, IsSubgroupChain and IsStdSubgroupChainRep ],
  function( f, ch )
    return IO_GenericObjectPickler(f,"CHAI",[],ch,[],[],
               ["slp","r","sgen","spos","size","height"]);
  end);

IO_Unpicklers.CHAI :=
  function(f)
    local ch;
    ch := rec();
    Objectify(StdSubgroupChainType,ch);
    return IO_GenericObjectUnpickler(f,ch,[],[]);
  end;
    
