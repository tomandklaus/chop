#############################################################################
##
##  chop.gi           chop package                           Max Neunhoeffer
##                                                              Felix Noeske
##
##  Copyright 2005 Lehrstuhl D f�r Mathematik, RWTH Aachen
##
##  Implementation stuff for chop
##
#############################################################################

# Dimension from which on no minimal polynomial is computed for an
# algebra element during splitting a module:
InstallValue( CH0P_GlobalOpts, rec( MinPolyLimit := 100 ) );

InstallValue( ModuleType, 
    NewType( ModulesFamily, IsModule and IsAttributeStoringRep ) );

# one can now create modules by doing:
# r := rec( ... )
# Objectify(ModuleType,r);

InstallMethod( Module, "for a list of matrices and an options record",
   [ IsList, IsRecord ],
   function( gens, opt )
     local m,f;
     if Length(gens) = 0 then
         Error("Usage: Module( matrices [,options] )");
         return;
     fi;
     f := BaseDomain(gens[1]);
     if not IsMutable( gens ) then gens := ShallowCopy( gens ); fi;
     m := rec( matrices := gens, field := f );
     # Now we need a word generator:
     if IsBound( opt.wordgenerator ) then
         m.wg := opt.wordgenerator;
     elif IsBound( opt.wordgeneratortype ) then
         m.wg := WordGenerator( gens, opt.wordgeneratortype );
     else
         m.wg := WordGenerator( gens, IsWordGeneratorGAP );
     fi;
     ObjectifyWithAttributes(m,ModuleType,
            MemoryPerRow, Memory( gens[1][1] ),
	    MemoryPerMatrix, Memory( gens[1] ),
	    Dimension, Length( gens[1] ) );
     if Dimension( m ) = 1 then # 1-dim
       SetIsSimpleModule( m, true );
       SetDegreeOfSplittingField( m, 1 );
       SetIdWordInfo( m, rec( word := CurrentWord(m!.wg),
                              goodfactor := 0*X( f ),
                              charpoly := X( f ) ) );
       SetFilterObj(m,IsInStandardBasis);
       SetSpinUpScript(m,rec( v := [], g := [] ));
       if ForAll(gens,IsOne) then
           SetFilterObj(m,IsTrivialModule);
       fi;
     fi;
     return m;
   end );

InstallMethod( Module, "for a list of matrices",
  [ IsList ],
  function( gens )
    if Length(gens) = 0 then
        Error("Usage: Module( matrices [,options] )");
        return;
    fi;
    return Module( gens, rec( ) );
  end );

InstallMethod( ViewObj, "for a module",
  [ IsModule ],
  function( m )
    Print( "<" );
    if not IsZeroModule( m ) then
      if IsTrivialModule(m) then
        Print("trivial ");
      elif HasIsSimpleModule(m) then
        if IsSimpleModule(m) then
          if HasDegreeOfSplittingField(m) and DegreeOfSplittingField(m) = 1 then
              Print("abs. ");
          fi;
          Print("simple ");
        else
          Print("reducible ");
        fi;
      fi;
      Print("module of dim. ",Dimension(m)," over ",m!.field);
      if HasIsSimpleModule(m) and HasDegreeOfSplittingField(m) and
         DegreeOfSplittingField(m) > 1 then
          Print(" splitting field degree ",DegreeOfSplittingField(m));
      fi;
    else
      Print("zero module");
    fi;
    Print(">");
  end );

InstallMethod( BaseField, "for a module",
  [ IsModule ],
  function( m )
    return m!.field;
  end );

InstallMethod( RepresentingMatrices, "for a module",
  [ IsModule ],
  function( m )
    return m!.matrices;
  end );

InstallMethod( \=, "for two modules", [IsModule, IsModule],
  function( m1, m2 )
    return m1!.matrices = m2!.matrices;
  end );

InstallOtherMethod( Memory, "for a module", [IsModule],
  function( m )
    local counted,i,j,matmem,mem;
    mem := 0;
    matmem := MemoryPerMatrix( m );
    for i in [1..Length(m!.matrices)] do
        if IsBound(m!.matrices[i]) then
            mem := mem + matmem;
        fi;
    od;
    mem := mem + Memory(m!.wg);  # the word generator cache
    if IsBound(m!.basis) then mem := mem + matmem; fi;
    if IsBound(m!.theta) then mem := mem + matmem; fi;
    return mem;
  end );


InstallGlobalFunction( CH0P_Spin, function( gens, basis, dim, seed )
  local old, counter, im, g, report;
  # INPUT
  # gens  : matrices
  # basis : record with fields
  #         pivots  : integer list giving pivot columns
  #         vectors : basis vectors in semi-echelon form
  # dim   : the dimension of the subspace to spin up, if unkown set to
  #         maximal dimension
  # seed  : seed vector of same length as rows of gens
  # OUTPUT
  # returns nothing.
  # changes basis to basis of smallest subspace
  # invariant under gens containing 'basis' and 'seed'
  # NOTES
  # 'basis' is assumed to span an invariant space
  # destructive in arguments 'basis' and 'seeds'
  old := Length( basis!.vectors );
  report := Maximum(100,QuoInt(dim,20));
  CleanRow( basis, seed, true, fail );
  counter := Length( basis!.vectors );
  if old = counter then 
    return true; #quick exit
  fi;
  while counter <= Length( basis!.vectors ) and 
        Length( basis!.vectors ) < dim do
    for g in gens do
      im := basis!.vectors[ counter ] * g;
      CleanRow( basis, im, true, fail );
    od;
    counter := counter + 1;
    if InfoLevel(InfoChop) > 2 and counter mod report = 1 then
        Info(InfoChop,3,"Spinning pos=",counter," have=",Length(basis!.vectors),
             " maxdim=",dim);
    fi;
  od;
  Info(InfoChop,3,"Spun up to dimension ",Length(basis!.vectors));
  return true;
end );

InstallGlobalFunction( CH0P_SpinStandardBasis, function( gens, dim, seed )
  local basis, counter, spv, spg, im, i, report;
  # INPUT
  # gens  : matrices
  # dim   : dimension of space to spin up (usually known)
  # seed  : seed vector of same length as rows of gens
  # OUTPUT
  # returns standard basis of smallest subspace
  # invariant under gens containing 'seed'
  # this is only a matrix! (no pivots or funny stuff!)
  # NOTES
  # to conserve memory we conduct a normal spin using semi-echelon basis and
  # record how to construct the basis. Then we quickly build the standard
  # basis using this knowledge (aka a spin-up script)
  report := Maximum(100,QuoInt(dim,20));
  if not IsZero( seed ) then #be mindful of dumb users
    basis := EmptySemiEchelonBasis( gens[ 1 ] );
    CleanRow( basis, ShallowCopy( seed  ), true, fail );
    counter := 1;
    spv := [];
    spg := [];
    while counter <= Length( basis!.vectors ) and 
          Length( basis!.vectors ) < dim do
	for i in [1..Length( gens )] do
            im := basis!.vectors[ counter ] * gens[ i ];
            if CleanRow( basis, ShallowCopy( im ), true, fail ) = false then
              Add( spv, counter );
              Add( spg, i );
            fi;
        od;
        if InfoLevel(InfoChop) > 2 and counter mod report = 1 then
            Info(InfoChop,3,"Spinning pos=",counter," have=",
                 Length(basis!.vectors)," maxdim=",dim);
        fi;
        counter := counter + 1;
    od;
    dim := Length(basis!.vectors);  # in case it is smaller
    basis := Matrix( [ seed ], Length( seed ), basis!.vectors );
    for i in [ 1 .. dim - 1 ] do
      Add( basis, basis[ spv[ i ] ] * gens[ spg[ i ] ] );
    od;
    Info(InfoChop,3,"Spun up to dimension ",Length(basis!.vectors));
    return basis;
  else
    return EmptySemiEchelonBasis( gens[ 1 ] );
  fi;
end );

InstallGlobalFunction( CH0P_SpinUpScript, function( gens, dim, seed )
  local spinupscript, basis, counter, im, i;
  # INPUT
  # gens : list of matrices
  # dim : dimension of subspace, if unknown set to maximum
  # seed : seed vector for spin
  # OUTPUT
  # spin up script: record with fields
  #      v : integer list, g : integer list
  # returns a spin up script for the invariant subspace
  # NOTES
  # we could make it destructive in seed
  spinupscript:=rec( v:=[], g:=[] );
  if not IsZero( seed ) then #be mindful of dumb users
    basis := EmptySemiEchelonBasis( gens[1] );
    CleanRow( basis, ShallowCopy( seed  ), true, fail );
    counter := 1;
    while counter <= Length( basis!.vectors ) and 
          Length( basis!.vectors ) < dim do
	for i in [1..Length( gens )] do
	im := basis!.vectors[ counter ] * gens[ i ];
	if CleanRow( basis, ShallowCopy( im ), true, fail ) = false then
	  Add( spinupscript.v, counter );
	  Add( spinupscript.g, i );
	fi;
      od;
      counter := counter + 1;
    od;
  fi;
  return spinupscript;
end );

InstallGlobalFunction( CH0P_SpinWithScript, function( gens, seed, script )
  local dim, basis, i;
  # INPUT
  # gens : list of matrices
  # seed : seed vector for spin
  # script : record, output of SpinUpScript
  # OUTPUT
  # matrix giving basis spun up according to script
  # NOTES
  #
  dim := Length( script.v ) + 1;
  basis := Matrix( [ seed ], Length(seed), gens[1] );
  for i in [ 1 .. dim - 1 ] do
    Add( basis, basis[ script.v[ i ] ] * gens[ script.g[ i ] ] );
  od;
  return basis;
end );

InstallGlobalFunction( CH0P_ActionOnSubspace, function( gens, basis )
  local zerov, imgens, w, i, v;
  # INPUT
  # gens  : List of matrices
  # basis : basis of invariant subspace ie record with fields
  #         pivots   : integer list of pivot columns
  #         vectors : matrix of basis in semi-echelon form
  # OUTPUT
  # List of matrices representing the action of the module given by 'gens'
  # on the Subspace given by 'basis'
  zerov := ZeroVector( Length( basis!.vectors ),
	               basis!.vectors ); #prepare vector type
  imgens := []; # stores result
  for i in [ 1 .. Length( gens ) ] do
    imgens[ i ] := Matrix( [], Length( basis!.vectors), basis!.vectors );
    for v in basis!.vectors do
      w := ShallowCopy( zerov );
      CleanRow( basis, v * gens[ i ], false, w );
      Add( imgens[ i ], w );
    od;
  od;
  return imgens;
end );

InstallGlobalFunction( CH0P_ExtendBasis, function( basis )
  local zerov, one, w, i;
  # INPUT
  # subspacebasis : record with fields
  #                 pivots   : integer list of pivot columns
  #                 vectors : matrix of basis in semi-echelon form
  # OUTPUT
  # returns nothing.
  # changes basis to record of whole space basis in semi-echelon form
  # NOTES
  # extends a basis of a subspace to a basis of the whole space by appending
  # appropriate standard basis vectors and thus yielding a basis of the
  # quotient space.
  # Is destructive
  zerov := ZeroVector( RowLength(basis!.vectors), basis!.vectors );
  one := One( zerov[ 1 ] );
  for i in Difference( [ 1..RowLength( basis!.vectors ) ], basis!.pivots ) do
    Add( basis!.pivots, i );
    w := ShallowCopy( zerov );
    w[ i ] := one;
    Add( basis!.vectors, w );
  od;
end );

InstallGlobalFunction( CH0P_ActionOnQuotient, function( gens, basis )
  local dimsubspc, dimfullspc, dimquotspc, diff, zerov, imgens, x, i, k;
  # INPUT
  # gens : List of matrices
  # basis : basis of submodule ie record with fields
  #         pivots   : integer list of pivot columns
  #         vectors : matrix of basis in semi-echelon form
  # OUTPUT
  # List of matrices representing the action of the module given by 'gens'
  # on the quotient space induced by 'basis'
  # NOTES
  # 
  dimsubspc := Length( basis!.vectors );
  dimfullspc := RowLength( basis!.vectors );
  dimquotspc := dimfullspc - dimsubspc;
  diff := Difference( [1..dimfullspc], basis!.pivots );
  zerov := ZeroVector( dimquotspc, basis!.vectors ); 
	               #prepare vector type
  imgens := []; # stores result
  for i in [ 1 .. Length( gens ) ] do
    #imgens[ i ] := MatrixNC( [], zerov );
    imgens[ i ] := ZeroMatrix( dimquotspc, dimquotspc, basis!.vectors ); #FIXME
    # OK now?
    # grab rows corresponding to added basis vectors:
    x := MutableCopyMat(gens[ i ]{ diff }); 
    for k in [1..Length(x)] do # clean rows with subspace basis
      CleanRow( basis, x[ k ], false, fail );
    od;
    # now remove zero columns
    CopySubMatrix( x, imgens[ i ], [1..dimquotspc], [1..dimquotspc], 
                                   diff , [1..dimquotspc] );
  od;
  return imgens;
end );

InstallGlobalFunction( CH0P_OrthogonalVector, function( b ) 
  # INPUT
  # b : semi echelon basis with components "vectors" and "pivots"
  #     Length(b!.vectors) must be < Length(b!.vectors[1]) > 0
  # OUTPUT
  # a vector that is orthogonal to all basis vectors in b with respect to
  # the standard scalar product
  local dim,i,p,poss,v,val,z;
  v := ZeroVector( RowLength(b!.vectors), b!.vectors );
  z := Zero( v[ 1 ] );
  dim := Length( v );
  # Find unused pivot:
  i := dim;
  while true do
      if not(i in b!.pivots) then break; fi;
      i := i - 1;
  od;
  # Now start building the vector:
  v[ i ] := One( v[ 1 ] );
  poss := [ i ];
  for i in [Length(b!.vectors),Length(b!.vectors)-1..1] do
      val := z;
      for p in poss do
          val := val + b!.vectors[i][p] * v[p];
      od;
      if not IsZero( val ) then #performance increase?
        v[b!.pivots[i]] := -val;
        Add(poss,b!.pivots[i]);
      fi;
  od;
  return v;
end );

InstallMethod( ProperSubmodule, "for a module", [IsModule], 
function( m ) 
  return ProperSubmodule( m, [], [] ); 
end);

InstallMethod( ProperSubmodule, "for an irreducible module and a list of hints",
               [IsModule and IsSimpleModule, IsList, IsList],
function( m, hintwords, hintfactors )
  return fail;
end );

InstallMethod( ProperSubmodule, "for a module and a list of hints",
               [IsModule, IsList, IsList], 
function( m, hintwords, hintfactors )
  local FindFactorsToTry,FindIrredFactors,N,algel,charpoly,deg,dim,fac,
        factorstotry,haveusedhintfactor,hint,mat,ndim,nsp,onemat,pr,res,
        subbasis,tgens,transposedgens,v,maxdeg;
  # INPUT
  # m : module
  # OUTPUT
  # fail   : m is simple
  # sbasis : basis for proper submodule
  # NOTES
  # 1. generate random word
  # 2. determine good factor of charpoly
  # 3. evaluate matrices in factor
  # 4. determine kernel
  # 5. spin up vector, transpose if nec and spin again
  
  maxdeg := 2;  # this grows with every algebra word tried

  FindIrredFactors := function( pr, polylist, onlydegs )
    local f,i,newf;
    f := [];
    i := 1;
    while i <= Length(polylist) do
        newf := Filtered(Factors(pr,polylist[i] : onlydegs := onlydegs),
                         x->DegreeOfLaurentPolynomial(x) in onlydegs);
        polylist[i] := polylist[i] / Product(newf);
        if Degree(polylist[i]) = 0 then
            Remove(polylist,i);
        else
            i := i + 1;
        fi;
        Append(f,Filtered(newf,p->Degree(p) <> 0));
    od;
    return Collected(f);
  end;
         
  FindFactorsToTry := function(algel,pr)
    local cp,factors,mp,onebad;
    if Length(algel) > CH0P_GlobalOpts.MinPolyLimit then
        # "Big" mode, we just consider low degree factors occurring once
        # in the characteristic polynomial:
        # Calculate characteristic polynomial:
        Info(InfoChop,3,"Computing characteristic polynomial...");
        cp := CharacteristicPolynomialOfMatrix( algel );
        #factors := FindIrredFactors(pr,cp.factors,[1..3]);
        factors := FindIrredFactors(pr,cp.factors,[1..12]); #FIXME
        # Note: It is cheaper to evaluate a new algebra element than
        # to evaluate the current one in a polynomial of degree > 2!
        # Taking degree 3 is a compromise!
        res := rec(charpoly := cp.poly);
        res.factorstotry := List(Filtered(factors,x->x[2] = 1),y->y[1]);
        onebad := First(factors,x->x[2] > 1);
        if onebad <> fail then Add(res.factorstotry,onebad[1]); fi;
    else
        # "Small" mode, we compute the minimal polynomial and take all
        # factors as "good", that occur with the same multiplicity in both
        # the minimal and the characteristic polynomial. No harm is done
        # by using the faster Monte Carlo method for the minimal polynomial,
        # since the worst case is that we "forget" to take some factor
        # because the Monte Carlo method estimates its multiplicity in the
        # minimal polynomial too small:
        Info(InfoChop,3,"Computing minimal polynomial...");
# the folloiwng routine is undefined deletd the Of. Klaius.
          mp := MinimalPolynomialOfMatrixMC(algel,1/10,1);
#       mp := CVEC_MinimalPolynomialMC(algel,1/10,true,false,1);
        res := rec( charpoly := mp.charpoly );
        res.factorstotry := mp.irreds{Filtered([1..Length(mp.irreds)],
            i->mp.mult[i] = mp.multmin[i] and Degree(mp.irreds[i]) <= maxdeg)};
        onebad := First([1..Length(mp.irreds)], i->mp.mult[i] > mp.multmin[i]);
        if onebad <> fail then Add(res.factorstotry,mp.irreds[onebad]); fi;
    fi;
    Info(InfoChop,3,"Factors to try: ",res.factorstotry);
    return res;
  end;

  transposedgens := false;
  onemat := One( RepresentingMatrices( m )[ 1 ] );
  pr := PolynomialRing(BaseDomain(RepresentingMatrices( m )[1]));
  dim := Dimension( m );
  hint := 1;
  while true do # not yet done
    # get new algebra element
    if hint <= Length( hintwords ) then
      SetWord( m!.wg, hintwords[ hint ] );
    elif hint = Length( hintwords ) + 1 then
      SetWord( m!.wg , fail );
      NextWord( m!.wg );
    else
      NextWord( m!.wg );
    fi;
    Info(InfoChop,3,"Evaluating algebra word...");
    algel := EvaluateWord( m!.wg );
    
    res := FindFactorsToTry(algel,pr);
    charpoly := res.charpoly;
    factorstotry := res.factorstotry;

    # now loop over factors to try:
    haveusedhintfactor := not(IsBound(hintfactors[hint]));
    while Length( factorstotry ) > 0 do
      if not(haveusedhintfactor) and 
         IsZero(charpoly mod hintfactors[hint]) then
          fac := hintfactors[hint];
          Info(InfoChop,3,"Using hinted factor ",fac);
          haveusedhintfactor := true;
      else
          fac := Remove(factorstotry,1);
          Info(InfoChop,3,"Using factor: ",fac);
      fi;
      # check if poly is irred => m is simple
      if DegreeOfLaurentPolynomial(fac) = dim then
          # Module is simple:
          SetIsSimpleModule( m, true );
          # zero is an IdWord, 
          # choose first standard basis vector for nullvector
          m!.theta := algel;
          m!.N := EmptySemiEchelonBasis( onemat );
          m!.N!.pivots:=1*[1..Dimension(m)];
          m!.N!.vectors:=MutableCopyMat(onemat);
          m!.thetaword := CurrentWord( m!.wg );
          m!.thetacharpoly := charpoly;
          m!.thetagoodfactor := 0*X( BaseField( algel ) );
          return fail;
      fi;
      deg := DegreeOfLaurentPolynomial( fac );
      # evaluate word
      Info(InfoChop,3,"Evaluating factor at algebra element...");
      mat := Value( fac, algel, onemat );
      Info(InfoChop,3,"Computing nullspace...");
      N := SemiEchelonBasisNullspace( mat );
      v := N!.vectors[ 1 ];
      ndim := Length( N!.vectors );
      subbasis := EmptySemiEchelonBasis( N!.vectors );
      CH0P_Spin( RepresentingMatrices( m ), subbasis, dim, v );
      if Length( subbasis!.vectors ) < dim then # reducible
        SetIsSimpleModule( m, false );
        m!.thetagoodfactor := fac;
	return subbasis;
      elif ndim = deg then # good factor
	# only one vector of nullspace needs to be checked
	# consider dual
	mat := TransposedMat( mat );
	if transposedgens = false then
	  tgens := List( RepresentingMatrices( m ), TransposedMat );
	  transposedgens := true;
	fi;
        Info(InfoChop,3,"Computing nullspace of transposed matrix...");
	nsp := NullspaceMatMutable( mat );
	subbasis := EmptySemiEchelonBasis( N!.vectors );
	CH0P_Spin( tgens, subbasis, dim, nsp[ 1 ] );
	if Length( subbasis!.vectors ) < dim then
	  v := CH0P_OrthogonalVector( subbasis );
	  subbasis := EmptySemiEchelonBasis( N!.vectors );
	  CH0P_Spin( RepresentingMatrices( m ), subbasis, dim, v );
          SetIsSimpleModule( m, false );
          m!.thetagoodfactor := fac;
	  return subbasis;
	else # m is proven to be simple with good factor
	     # store algel for absolute irred test
          SetIsSimpleModule( m, true );
	  m!.theta := algel;
	  m!.N := N;
	  m!.thetaword := CurrentWord( m!.wg );
	  m!.thetacharpoly := charpoly;
	  m!.thetagoodfactor := fac;
	  return fail;
	fi;
      fi;
    od; # take next factor
    hint := hint + 1;
    if maxdeg <= 8 then maxdeg := maxdeg * 2; fi;
  od;
end);

# SetDegreeOfSplittingField( m, e );
InstallGlobalFunction( CH0P_SpinSubspace, function( gens, basis, dim, seeds )
  local counter, im, i, g, report;
  # WILL REPLACE CH0P_Spin. NOW ALLOWS LIST OF SEEDS
  # 
  # INPUT
  # gens  : matrices
  # basis : record with fields
  #         pivots  : integer list giving pivot columns
  #         vectors : basis vectors in semi-echelon form
  # dim   : the dimension of the subspace to spin up, if unkown set to
  #         maximal dimension
  # seeds  : list of seed vectors of same length as rows of gens
  # OUTPUT
  # returns nothing.
  # changes basis to basis of smallest subspace
  # invariant under gens containing 'basis' and 'seeds'
  # NOTES
  # 'basis' is assumed to span an invariant space
  # destructive in arguments 'basis' and 'seeds'

  # clean out seeds and add them to basis
  report := Maximum(100,QuoInt(dim,20));
  counter := Length( basis!.vectors ) + 1; # points to first appended seed
  for i in [1..Length(seeds)] do
    CleanRow( basis, seeds[i], true, fail );
  od;
  
  while counter <= Length( basis!.vectors ) and 
        Length( basis!.vectors ) < dim do
    for g in gens do
      im := basis!.vectors[ counter ] * g;
      CleanRow( basis, im, true, fail );
    od;
    if InfoLevel(InfoChop) > 2 and counter mod report = 1 then
        Info(InfoChop,3,"Spinning pos=",counter," have=",
             Length(basis!.vectors)," maxdim=",dim);
    fi;
    counter := counter + 1;
  od;
  return true;
end );
	

InstallMethod( DegreeOfSplittingField, "for a module", [IsModule],
  function( m )
    if ProperSubmodule( m ) <> fail then
        return fail;
    else
        # Call operation again, now with m known to be simple:
        return DegreeOfSplittingField( m );
    fi;
  end );


InstallMethod( DegreeOfSplittingField, "for a simple module",
               [IsModule and HasIsSimpleModule and IsSimpleModule],
function( m )
  local IsExtendible, q, d, f, g, eprime, tries, zerovf, 
        thetaonN, tau, v, specialbasismat, imv, mat, sigma, mu, i, k;
  # INPUT
  # m : simple module
  # OUTPUT
  # e : degree of splitting field
  # NOTES
  # sets attribute DegreeOfSplittingField
  # 1. d = Dimension( m ), 
  #    f := dim N, ie dimension of nullspace of splitword theta 
  #                evaluated in good factor
  # 2. g := gcd( d, f ) 
  #         ( degree of splitting field is a divisor of both d and f, 
  #           as both are vector spaces over the splitting field too )
  #
  #    ** The idea is now as follows: we seek to determine a generator 
  #       sigma of the splitting field End_FG(m), ie an endomorphism 
  #       whose action commutes with the action induced by all 
  #       group elements. We do this by firstly decreasing the problem 
  #       size: instead of the full space m we consider the nullspace 
  #       N and consider the endomorphism ring of N viewed as a <theta>-
  #       module. As N is an irreducible <theta>-module (the characteristic
  #       polynomial is the good factor and therefore irreducible),
  #       End_theta(N) is a field. Now, since V > N and FG > F<theta>
  #       the splitting field End_FG(V) sought is a subfield of 
  #       End_theta(N).
  #       Therefore we first find an element sigma which commutes with 
  #       theta, i.e. sigma is in the field End_theta(N).
  #       Assuming e' to be the splitting field degree, we check if 
  #       sigma has the order q^e'-1 of the splitting field. If this is not
  #       the case, we select a new random sigma in End_theta(N).
  #       If sigma has the proper order, we check if its theta-invariant 
  #       action on N can be extended to a G-invariant action on m. 
  #       If so, then we have found a generator of End_FG(m). 
  #       If not, then the splitting field degree must be smaller than e': 
  #       sigma is a primitive element which generates a subfield of 
  #       the cyclic Galois extension End_theta(N) over F. Hence
  #       sigma generates the unique subfield in End_theta(N) of degree e' 
  #       over F. But as sigma is not extendible to m, the corresponding 
  #       field cannot be the splitting field.
  #
  # 3. For all e' | g in order of decreasing size
  #    3.1 choose random tau in End_theta(N) ( begin with theta )
  #        ( choose random element by
  #          choosing arbitrary v in N and spinning with theta, ie
  #          tau maps standard basis e_1, ..., e_f of N to
  #                  [v, v*theta, v*theta^2, ... , v*theta^(f-1)] )
  #
  #        ** Note that N is an irreducible <theta>-module, as the minimal 
  #           polynomial of theta on N is the good factor, which is 
  #           irreducible.
  #
  #        calculate representation for tau in new basis, gives fxf matrix
  #    3.2 set sigma := tau^(q^f-1)/(q^e'-1) ( now |sigma| | q^e'-1 )
  #    3.3 Calc mu := Minpol( sigma ) ( thus |sigma| = q^(deg mu) - 1 )
  #        if deg mu < e' then choose new random tau (step 3.1)
  #        ( else sigma has order q^e'-1 )
  #    3.4 determine if sigma is extendible to M:
  #        take initial (semi echelon) basis of N
  #        spin it up with matrices of algebra using DecomposeAndExtend
  #        mirror spinning on image of basis under sigma (call this rhs)
  #        if spun up vector w decomposes take its decomposition and multiply
  #        it by rhs basis giving w'.
  #            if sigma( w ) = w' then continue spinning
  #            else sigma( w ) <> w' and sigma is not extendible to M
  #    3.5 Now: if sigma is extendible then e' = e. we are done
  #             else e < e', choose next e' (step 3)
  IsExtendible:=function( endo )
    local counter, workN, sigmaN, dec, im, im2, g;
    counter := 1;
    workN := StructuralCopy( m!.N );
    #workN := rec( vectors:=MutableCopyMat( m!.N!.vectors ),
    #	          pivots := ShallowCopy( m!.N!.pivots ),
    #		  helper := m!.N!.helper ); # preserve N
    sigmaN := endo * workN!.vectors;
    dec := ZeroMutable( m!.matrices[ 1 ][ 1 ] );
    while counter <= Length( workN!.vectors ) do 
      for g in m!.matrices do
	im := workN!.vectors[ counter ] * g;
	im2 := sigmaN[ counter ] * g;
	if CleanRow( workN, im, true, dec ) then
	  if dec{ [ 1..Length( workN!.vectors ) ] } * sigmaN <> im2 then
	    return false;
	  fi;
	else
	  Add( sigmaN, dec[ Length( workN!.vectors ) ]^-1 *
	     ( im2 - dec{ [ 1..Length( workN!.vectors ) - 1 ] } * sigmaN ) );
	fi;
      od;
      counter := counter + 1;
    od;
    return true;
  end;
  
  q := Size( BaseField( RepresentingMatrices( m )[ 1 ] ) );
  d := Dimension( m );
  f := Length( m!.N!.vectors ); # = deg p
  g := GcdInt( d, f );
  eprime := DivisorsInt( g );
  tries := 1;
  #zerovd := ZeroMutable( m!.matrices[1][1] );
  zerovf := ZeroVector( f, m!.matrices[1] );
  for i in [ Length(eprime), Length(eprime) - 1 .. 2] do
    while true do
      if tries = 1 then
	if eprime[ i ] = d then # kernel is everything
	  # and charpoly is irred
	  thetaonN := [ m!.theta ];
	else
	  thetaonN := CH0P_ActionOnSubspace( [ m!.theta ], m!.N );
	fi;
	tau := thetaonN[ 1 ];
      else
        v := ShallowCopy( zerovf ); #erste Zeile tau
	v[ 1 ] := One( BaseField( v ) );
	specialbasismat := Matrix( [ v ], Length(v), m!.matrices[1] );
	repeat
	  imv := Randomize( zerovf );
	until not IsZero( imv );
	mat := Matrix( [ imv ], Length(imv), m!.matrices[1] );
	for k in [2..f] do
	  specialbasismat[ k ] := specialbasismat[ k - 1 ] * thetaonN[ 1 ];
	  imv := imv * thetaonN[ 1 ];
	  mat[ k ] := imv;
	od;
	tau := specialbasismat^-1 * mat;
        # tau is the matrix of the endomorphism mentioned above with respect
        # to the standard basis e_1,..., e_n
      fi;
      tries := tries + 1;
      sigma := tau^( ( q^f - 1 ) / ( q^eprime[ i ] - 1 ) );
      mu := MinimalPolynomialOfMatrix( sigma );
      if DegreeOfLaurentPolynomial( mu ) < eprime[ i ] then
        continue;
      else
	break;
      fi;
    od;
    if IsExtendible( sigma ) then
      # if f = e' then theta is an idword and we should store it
      if f = eprime[ i ] then
	SetIdWordInfo(m, rec( word := m!.thetaword,
	                      goodfactor := m!.thetagoodfactor,
			      charpoly := m!.thetacharpoly ) );
	m!.nullvector := m!.N!.vectors[1];
      fi;
      # if f <> e' then delete m.theta and m.N now
      Unbind( m!.theta );
      Unbind( m!.thetaword );
      Unbind( m!.thetagoodfactor );
      Unbind( m!.thetacharpoly );
      Unbind( m!.N );
      return eprime[ i ];
    fi;
  od;
  if f = 1 then
    SetIdWordInfo(m, rec( word := m!.thetaword,
			  goodfactor := m!.thetagoodfactor,
			  charpoly := m!.thetacharpoly ) );
    m!.nullvector := m!.N!.vectors[1];
  fi;
  Unbind( m!.theta );
  Unbind( m!.thetaword );
  Unbind( m!.thetagoodfactor );
  Unbind( m!.thetacharpoly );
  Unbind( m!.N );
  return 1;
end
);

InstallMethod( IdWordInfo, "for a simple module with known splitting field",
  [ IsModule and IsSimpleModule and HasDegreeOfSplittingField ],
  function( m )
    local N,algel,cpfacs,e,fac,facsdege,i,x;
    Info( InfoChop, 2, "Looking for IdWord..." );
    # Unfortunately, the algel used to determine the degree of the splitting
    # field was no idword, so look for a real idword:
    e := DegreeOfSplittingField( m );
    while true do
        NextWord( m!.wg );
        algel := EvaluateWord( m!.wg );
        cpfacs := CharacteristicPolynomialOfMatrix( algel );
        facsdege := [];
        for fac in cpfacs.factors do
            Append(facsdege, 
	    Filtered( Factors( PolynomialRing(BaseField( algel ) ), 
	              fac : factoroptions := rec( onlydegs := [e] ) ), 
	              x->DegreeOfLaurentPolynomial(x) = e ));
		      # Eiertanz
	    
        od;
        if Length(facsdege) = 0 then
            Info( InfoChop, 3, "No factors of degree ", e, "!" );
            continue;
        fi;
        facsdege := Collected(facsdege);
        Info( InfoChop, 3, "Trying ", Length(facsdege), " different factors ",
              "of degree ",e," ...");
        for i in [1..Length(facsdege)] do
            x := Value( facsdege[i][1], algel );
            N := NullspaceMatMutableX(x);
            if Length(N) = e then
                # Found one!
                m!.nullvector := N[1];
                return rec( word := CurrentWord( m!.wg ),
                            goodfactor := facsdege[i][1],
                            charpoly := cpfacs.poly );
            fi;
        od;
    od;
  end );

InstallMethod( IdWordInfo, "for a simple module without known splitting field",
  [ IsModule and IsSimpleModule ],
  function ( m )
    DegreeOfSplittingField( m );
    # Do we happen to know an IdWord already?
    # Call the same operation again, now the method with degree works
    return IdWordInfo(m);
    # Note that DegreeOfSplittingField might have set IdWordInfo already!
  end );

InstallMethod( IdWordInfo, "for a module not known to be simple", [ IsModule ],
  function( m )
    local sub;
    sub := ProperSubmodule( m );
    if sub <> fail then
        Error( "Sorry, no IdWords for reducible modules possible!" );
    fi;
    # We call ourselves, now knowing that the module is simple:
    return IdWordInfo(m);
  end );

InstallMethod( TransformToStandardBasis, "usage info without arguments",
  [ ],
  function()
    Print("Usage: TransformToStandardBasis( <simple module> )\n");
    Print("       Note: module must have IdWordInfo!\n");
  end );

InstallMethod( TransformToStandardBasis, "for a simple module",
  [ IsModule and IsSimpleModule and HasIdWordInfo ],
  function( m )
  local stdbasis, istdbasis, i, spinupscript, saveword;
  #
  # transforms matrices of m to standard basis
  # sets object attribute SpinUpScript
  if IsInStandardBasis(m) then
      return One( m!.matrices[ 1 ] );
  fi;
  if Dimension( m ) = 1 then
    Unbind(m!.nullvector);
    SetSpinUpScript(m,fail);
    SetFilterObj( m, IsInStandardBasis );
    return One( m!.matrices[ 1 ] );
  fi;
  spinupscript := CH0P_SpinUpScript( m!.matrices, 
                                     Dimension( m ), m!.nullvector );
  stdbasis := CH0P_SpinWithScript( m!.matrices, m!.nullvector, spinupscript );
  Unbind(m!.nullvector);
  istdbasis := stdbasis^-1;
  for i in [1..Length(m!.matrices)] do
    m!.matrices[ i ] := stdbasis * m!.matrices[ i ] * istdbasis;
  od;
  saveword := CurrentWord( m!.wg );
  m!.wg := WordGenerator( m!.matrices, TypeOfWordGenerator( m!.wg ) );
  SetWord(m!.wg, saveword);
  SetFilterObj( m, IsInStandardBasis );
  SetSpinUpScript( m, spinupscript );
  return stdbasis;
end );

InstallGlobalFunction( CH0P_CopyModule, function( m )
  local mm;
  mm := Module( ShallowCopy( RepresentingMatrices( m ) ),
                rec( wordgeneratortype := TypeOfWordGenerator(m!.wg) ) );
  SetWord( mm!.wg, CurrentWord( m!.wg ) );
  # we do not copy the cache!
  if HasIsSimpleModule( m ) and IsSimpleModule( m ) then
      SetIsSimpleModule( mm, true );
      if not HasDegreeOfSplittingField( m ) then
          mm!.theta := m!.theta;
          mm!.N := m!.N;
          mm!.thetaword := m!.thetaword;
          mm!.thetacharpoly := m!.thetacharpoly;
          mm!.thetagoodfactor := m!.thetagoodfactor;
      else   # HasDegreeOfSplittingField:    
          SetDegreeOfSplittingField( mm, DegreeOfSplittingField( m ) );
          if HasIdWordInfo( m ) then
              SetIdWordInfo( mm, IdWordInfo( m ) );
              if IsInStandardBasis( m ) then
                  SetFilterObj( mm, IsInStandardBasis );
                  SetSpinUpScript(mm,SpinUpScript(m));
              else
                  mm!.nullvector := ShallowCopy(m!.nullvector);
              fi;
          fi;
      fi;
  elif HasIsSimpleModule(m) then
      if IsBound(m!.thetagoodfactor) then
          mm!.thetagoodfactor := m!.thetagoodfactor;
      fi;
  fi;
  return mm;
end );

InstallMethod( Isomorphism, 
  "for a simple module with known spl.fld.deg. and a module",
  [ IsModule and IsSimpleModule and HasDegreeOfSplittingField and
    HasIdWordInfo and IsInStandardBasis, IsModule ],
  function( m1, m2 )
    local algel, charpoly, onemat, word, N, basis, ibasis, i, saveword;
    # INPUT
    # irred modules m1 in stdbasis, m2 in any basis
    # OUTPUT
    # m1 isomorphic to m2 :
    #    basis change putting m2 in stdbasis  
    # m1 not isomorphic to m2 :
    #    fail
    # NOTES
    # We assume that m1 and m2 have the same number of matrices.
    # Procedure:
    # 1. check dims
    # 2. check degree of splitting field
    # 3. eval IdWord of m1 in m2, calc charpoly
    # 4. check charpolys
    # 5. insert m2 in charpoly, calc nullspace
    # 6. check nullspace dims
    # 7. spin with SpinUpScript(m1) nullspace vector of m2
    # 8. conjugate m2, check matrices
    # 9. return basis change or fail
    if Length( m1!.matrices ) <> Length( m2!.matrices ) then
      Error("Both modules must have the same number of representing ",
            "matrices.\n");
    fi;
    #
    if Dimension( m1 ) <> Dimension( m2 ) then
      return fail;
    elif HasDegreeOfSplittingField( m2 ) and
         DegreeOfSplittingField( m1 ) <> DegreeOfSplittingField( m2 ) then
      return fail;
    fi;
    if Dimension( m1 ) = 1 then
      if m1!.matrices = m2!.matrices then
	return OneMutable( m1!.matrices[ 1 ] );
      else
	return fail;
      fi;
    fi;
    saveword := CurrentWord(m2!.wg);
    SetWord( m2!.wg, m1!.IdWordInfo.word );
    algel := EvaluateWord( m2!.wg );
    SetWord( m2!.wg, saveword );
    # Now the cache is cleared in m2!.wg, if this costs performance,
    # we will also have to save the cache here...
    charpoly := CharacteristicPolynomialOfMatrix( algel );
    if charpoly.poly <> m1!.IdWordInfo.charpoly then
      return fail;
    fi;
    onemat := One( algel );
    if IsZero(IdWordInfo(m1).goodfactor) then
        N := onemat;
    else
        word := Value( IdWordInfo(m1).goodfactor, algel, onemat );
        N := NullspaceMatMutableX( word );
    fi;
      
    if Length( N ) <> DegreeOfSplittingField( m1 ) then
      return fail;
    fi;
    basis := CH0P_SpinWithScript( m2!.matrices, N[ 1 ], 
	                          SpinUpScript(m1) );
    ibasis := basis^-1;
    if ibasis = fail then
      return fail;
    fi;
    # changing matrices
    for i in [1..Length(m2!.matrices)] do
      if m1!.matrices[ i ] <> basis * m2!.matrices[ i ] * ibasis then
	return fail;
      fi;
    od;
    return basis;
  end );

InstallGlobalFunction( CH0P_CHOP, 
                       function( m, hints, destructive, opt, depth )
  local dim,gens,i,idw,iso,mm,olddim,qdim,qtemp,quotm,quotmgens,randvec,
        sdim,store,subbasis,subm,submgens,typeofwordgenerator;
  # INPUT
  # m : module, opt.db : database of known irred modules
  # hints : tree of words to use first
  # opt.compbasis : if true we compute adapted basis
  #               if false we dont
  # destructive : true or false, if false then the matrices of the
  #               module m are guaranteed to be left untouched, if true
  #               then the matrices will be transformed to standard basis
  #               if m is irreducible and not yet in opt.db
  #               if module is reducible the matrices are unbound
  # further options: splittree : if true, log what happens in split tree
  #                  ichopdepth : depth up to which random vectors are tries
  #                  ichoptries : number of random vectors to try
  # depth : zero for outermost call, is increased with every recursion
  # store a record to store answer. needs to be prepared in advance
  # store.db : link to opt.db
  # store.mult : multiplicities of opt.db entries as cf of m
  # store.acs : ascending composition series (list of ints)
  # OUTPUT
  # record giving multiplicities of cf (modules in database)
  # entry .module is the input module. Klaus
  # ascending composition series (list of positions referring to opt.db)
  # if basisswitch = true : adapted basis
  # NB database is always updated
  # NOTES
  # Some serious chopping
  # 1. test m for proper submodule, use hints if present
  # 2. if m irred then compare with modules in database
  # 3. if m new cf then determine splitting field, transform to std basis
  #    add to opt.db, update multiplicty counter, update adapted basis
  # 4. if m old update multiplicity counter, update basis
  # 5. branch: do action on submodule and call recursion
  # 6. branch: do action on quotient and call recursion
  dim := Dimension( m );
  gens := RepresentingMatrices(m);
  if depth < opt.ichopdepth and 
     dim >= Minimum(5,QuoInt(500,Size(m!.field))) then
      Info( InfoChop, 2, "Trying ",opt.ichoptries," random vectors to split ", 
            dim, "-dimensional module..." );
      for i in [1..opt.ichoptries] do
          subbasis := EmptySemiEchelonBasis( gens[1] );
          while Length(subbasis!.vectors) < QuoInt(dim,2) do
              randvec := ShallowCopy( gens[1][1] );
              Randomize(randvec);
              Info( InfoChop, 2, "Trying random vector for split..." );
              olddim := Length(subbasis!.vectors);
              CH0P_SpinSubspace( gens, subbasis, dim, [randvec] );
              Info( InfoChop, 2, "Found Subspace of Dimension ", 
                    Length( subbasis!.vectors ) );
          od;
          # Maybe one step back:
          if AbsInt(QuoInt(dim,2)-olddim) < 
             AbsInt(Length(subbasis!.vectors)-QuoInt(dim,2)) then
              subbasis!.vectors := subbasis!.vectors{[1..olddim]};
              subbasis!.pivots := subbasis!.pivots{[1..olddim]};
          fi;
	  if Length( subbasis!.vectors ) < 
	     dim - QuoInt( dim, 5 ) and
	     Length( subbasis!.vectors ) > 0 then
	      Info( InfoChop, 2, "It is small enough for our taste..." );
              break;
          else
              subbasis := fail;
          fi;
      od;
  else
      subbasis := fail;
  fi;
  
  if subbasis = fail then      
      Info( InfoChop, 2, "Trying to split ", dim,"-dimensional module..." );
      if hints = fail or Data( hints ) = fail then
        subbasis := ProperSubmodule( m, [], [] );
      else
        Info(InfoChop,3,"Using hints: ",Data(hints));
        subbasis := ProperSubmodule( m, [ Data( hints ).word ],
                                        [ Data( hints ).goodfactor ] );
      fi;
  fi;

  if subbasis = fail then # irred
    store := rec( ischoprecord := true,
                  db:= opt.db, mult := [1..Length(opt.db)]*0 );
    for i in [1..Length(store.db)] do
      iso := Isomorphism( store.db[ i ], m );
      if iso <> fail then # OLD
        Info( InfoChop, 1, "Found irreducible module Nr. ",i );
	store.mult[ i ] := store.mult[ i ] + 1;
	# apply iso to basis
	if opt.compbasis then # return basis change
	  store.basis := iso;
	fi;
	# dont store basis
	store.acs := [ i ];
        # module component in record store missing. Added it. Klaus
        # compare to the case below  when the simple module found is new.
        store.module:=m;
        idw := IdWordInfo( store.db[i] );
        if opt.splittree then
          store.splittree := BinaryTree( rec( word := StructuralCopy(idw.word),
                                              goodfactor := idw.goodfactor ) );
        fi;
	return store;
      fi;
    od; # done checking old, must be NEW
    Info( InfoChop, 1, "Found NEW irreducible of dimension ",
          Dimension( m ) );
    # Find IdWord, ie compute splitting field
    DegreeOfSplittingField( m );
    idw := IdWordInfo( m );   # this ensures, that now IdWordInfo is set!
    # update basis
    if destructive then
      mm := m;
    else
      # Here we need to make a new module:
      mm := CH0P_CopyModule( m );
    fi;
    iso := TransformToStandardBasis( mm );
    MakeImmutable( mm!.matrices );  # to notice further in place changes!
    Add( store.db, mm );
    store.mult[ Length( store.db ) ] := 1;
    if opt.compbasis then
      store.basis := iso;
    fi;
    store.acs:=[ Length( store.db ) ];
    store.module := m;
    if opt.splittree then
        store.splittree := BinaryTree( rec( word := StructuralCopy(idw.word),
                                            goodfactor := idw.goodfactor ) );
    fi;
    return store;
    # all done
  else # reducible
    sdim := Length( subbasis!.vectors );
    qdim := dim - sdim;

    Info( InfoChop, 2, "Split module into ", sdim," + ", qdim );
    # word no longer needed as matrix, we even delete the word generator:
    typeofwordgenerator := TypeOfWordGenerator(m!.wg);  # for later use
    if destructive then
        m!.wg := fail;
    else
        ClearCache(m!.wg);  # also to save memory
    fi;
    submgens := [ ];
    quotmgens := [ ];
    Info( InfoChop, 3, "Computing action on sub and factor space..." );
    for i in [ 1 .. Length( RepresentingMatrices( m ) ) ] do
      submgens[ i ] := CH0P_ActionOnSubspace( gens{[ i ]}, subbasis )[ 1 ];
      if destructive then
	quotmgens[ i ] := CH0P_ActionOnQuotient( gens{[ i ]}, subbasis )[ 1 ];
        Unbind( gens[ i ] );
      fi;
    od;
    # If destructive=true, then we no longer have access to the original
    # matrices to save memory (all depths >= 1 are destructive).
    # Note however that the subbasis is still needed and might use up
    # lots of memory if the chop tree is deep!
    
    subm := Module( submgens, 
                    rec( wordgeneratortype := typeofwordgenerator ) );
    
    if HasLeft( hints ) then
      store := CH0P_CHOP( subm, Left( hints ), true, opt, depth+1 );
    else
      store := CH0P_CHOP( subm, fail, true, opt, depth+1 );
    fi;
    Unbind( subm ); Unbind( submgens ); Unbind( store.module );
    if opt.compbasis then
      # calculate first part of basis
      store.basis := store.basis * subbasis!.vectors;
    fi;
    
    if not destructive then
      Info( InfoChop, 3, "Computing action on factor space..." );
      quotmgens := CH0P_ActionOnQuotient( gens, subbasis );
    fi;
    quotm := Module( quotmgens,
                     rec( wordgeneratortype := typeofwordgenerator ) );
    
    # Now subbasis!.vectors is obsolete N.B. subbasis.pivots will be used later
    Unbind( subbasis!.vectors );
    if HasRight( hints ) then
      qtemp := CH0P_CHOP( quotm, Right(hints), true, opt, depth+1 );
    else
      qtemp := CH0P_CHOP( quotm, fail, true, opt, depth+1 );
    fi;
    Unbind( quotm ); Unbind( quotmgens ); Unbind( qtemp.module );
       
    if opt.splittree then
        store.splittree := BinaryTree( rec( word := CurrentWord( m!.wg ),
                                            goodfactor := m!.thetagoodfactor ), 
                                       store.splittree, qtemp.splittree );
    fi;

    while Length( store.mult ) < Length( qtemp.mult ) do
      Add( store.mult, 0 );
    od;
    store.mult := store.mult + qtemp.mult; 
    Append( store.acs, qtemp.acs );
    if opt.compbasis then
      # calculate rest of basis
      for i in [ 1 .. qdim ] do
	Add( store.basis, ZeroMutable( store.basis[ 1 ] ) );
      od;
      CopySubMatrix( qtemp.basis, store.basis, 
		     [ 1 .. qdim ], [ sdim + 1 .. dim ], 
		     [ 1 .. qdim ], Difference( [ 1 .. dim ], 
						subbasis!.pivots ) );
    fi;
    store.module := m;
    return store;
  fi;
end );

InstallGlobalFunction( CH0P_SemiSimplicityBasis, function(re, chain)
  local gens, DoRecurse, re2, i, dimsubs;
  # INPUT 
  #       re  : record returned by chop with argument basischange = true
  #       chain : subgroups chain, given by PrepareChain
  # OUTPUT re : changes composition series basis to ssb
  #        re.gensinssb : matrices in ssb
  # NOTES
  #  

  # First do a standard Chop with basis:
  #re := Chop( m, rec( db := db, compbasis := true) );
  re.ibasis := re.basis^-1;
  gens := ShallowCopy( RepresentingMatrices( re.module ) );
  # transform to composition series basis
  for i in [1..Length(gens)] do
    gens[ i ] := re.basis * gens[ i ] * re.ibasis;
  od;
  
  # calc dims of submodules
  dimsubs := [0];
  for i in [1..Length( re.acs )] do
    Add(dimsubs, dimsubs[ i ] + Dimension( re.db[ re.acs[ i ] ] ) );
  od;
  
  
  DoRecurse := function(gens,dims)
    local l, i, d, dd, e, pi, pihut, ei, k, res, j;

    l := Length(dims);
    i := First([1..l],x->2 * dims[x] >= dims[l]);
    if dims[l] - dims[i-1] * 2 < dims[i] * 2 - dims[l] then
        i := i - 1;
    fi;
    Print("Chopping ",dims[l]," into ",dims[i],"+",dims[l]-dims[i],"...\n");
    d := dims[i];
    dd := dims[l];
    e := OneMutable(gens[1]);
    pi := e*0;
    CopySubMatrix( e, pi, [1..d], [1..d], [1..d], [1..d] );
    
    pihut := ChainWorker( gens, chain, DoworkMaschke, pi ) * 
	     ( One( BaseField( gens[1] ) ) * Size(chain) )^-1;
    e{[d+1..dd]} := e{[d+1..dd]} * (e - pihut);
    ei := e^-1;
    for k in [1..Length(gens)] do
      gens[ k ] := e * gens[ k ] * ei;
    od;
    #gens := List( gens, x-> e*x*ei);
    # Do the submodule:
    if i > 2 then
        Info( InfoChop, 1, "Handling submodule:\n");
	res := DoRecurse(List(gens,x->ExtractSubMatrix(x, [1..d], [1..d])),
	                 dims{[1..i]});
        for j in [1..Length(gens)] do
	  CopySubMatrix( res.gens[ j ], gens[ j ],
	                 [1..d], [1..d], [1..d], [1..d] );
        od;
	CopySubMatrix( res.bas, e, [1..d], [1..d], [1..d], [1..d] );
        CopySubMatrix( res.basi, ei, [1..d], [1..d], [1..d], [1..d] );
	CopySubMatrix( ExtractSubMatrix( ei, [d+1..dd], [1..d] ) * res.basi,
	               ei, [1..dd-d], [d+1..dd], [1..d], [1..d] );
    fi;

    # Do the complement:
    if i < l-1 then
        Info( InfoChop, 1, "Handling factor module:\n");
        res := DoRecurse( List( gens,
	       x->ExtractSubMatrix(x, [d+1..dd],[d+1..dd] ) ),
	       dims{[i..l]}-d );
        for j in [1..Length(gens)] do
	  CopySubMatrix( res.gens[j], gens[j], [1..dd-d], [d+1..dd],
	                 [1..dd-d], [d+1..dd] );
        od;
        e{[d+1..dd]} := res.bas * e{[d+1..dd]};
	CopySubMatrix( ExtractSubMatrix( ei, [d+1..dd], [d+1..dd] ) *
	res.basi, ei, [1..dd-d], [d+1..dd], [1..dd-d], [d+1..dd] );
    fi;

    return rec(gens := gens, bas := e, basi := ei);
  end;
  
  re2 := DoRecurse( gens, dimsubs );
  re.basis := re2.bas * re.basis;
  re.ibasis := re.ibasis * re2.basi;
  re.gensinssb := re2.gens;
  return re;
end );

InstallMethod( Chop, "for a module object", [ IsModule ],
  function( m )
    return Chop( m, rec() );
  end );

InstallMethod( Chop, "for a module object", [ IsModule, IsRecord ],
  function( m, r )
    if not(IsBound(r.db)) then r.db := []; fi;
    if not(IsBound(r.hints)) then r.hints := fail; fi;
    if not(IsBound(r.compbasis)) then r.compbasis := false; fi;
    if not(IsBound(r.destructive)) then r.destructive := false; fi;
    if not(IsBound(r.splittree)) then r.splittree := false; fi;
    if not(IsBound(r.ichopdepth)) then r.ichopdepth := 0; fi;
    if not(IsBound(r.ichoptries)) then r.ichoptries := 5; fi;
    if Length( RepresentingMatrices( m ) ) = 0 then
      Error("Module has no matrices");
    fi;
    return CH0P_CHOP( m, r.hints, r.destructive, r, 0);
  end );

InstallMethod( Chop, "usage display without arguments", [],
  function( )
    Print("Usage: Chop( <module> [, <optrec>] )   where\n");
    Print("  <module> is a module object and\n");
    Print("  <optrec> is a record that can have the following ",
          "components bound:\n");
    Print("    db : a database of simple modules in standard basis ",
          "(default [])\n");
    Print("    hints : a hint tree, see documentation (default fail)\n");
    Print("    compbasis   : a boolean value indicating whether a basis ",
          "change matrix \n                  is computed (default false)\n");
    Print("    destructive : a boolean value indicating whether the\n");
    Print("                  matrices may be changed (default false)\n");
    Print("    splittree   : a boolean value indicating whether a\n");
    Print("                  splittree is returned recording everything\n");
  end );

InstallMethod( Display, "for a chop record",
  [ IsRecord ],
  function( r )
    local i,j,m;
    if not(IsBound(r.ischoprecord)) then TryNextMethod(); fi;
    Print("chop record for ");
    ViewObj(r.module);
    Print("\n\ndb:");
    i := 1;
    while i <= Length(r.db) do
        Print("\n#   ");
        m := Minimum(i+11,Length(r.db));
        for j in [i..m] do Print(String(j,6)); od;
        Print("\ndim ");
        for j in [i..m] do Print(String(Dimension(r.db[j]),6)); od;
        Print("\nmult");
        for j in [i..m] do 
            if IsBound(r.mult[j]) then
                Print(String(r.mult[j],6)); 
            else
                Print("     *");
            fi;
        od;
        Print("\nsplt");
        for j in [i..m] do Print(String(DegreeOfSplittingField(r.db[j]),6)); od;
        Print("\n");
        i := i + 12;
    od;
    Print("\nacs:");
    i := 1;
    while i <= Length(r.acs) do
        Print("\niso ");
        m := Minimum(i+11,Length(r.acs));
        for j in [i..m] do Print(String(r.acs[j],6)); od;
        Print("\ndim ");
        for j in [i..m] do Print(String(Dimension(r.db[r.acs[j]]),6)); od;
        Print("\n");
        i := i + 12;
    od;
  end );
        
InstallMethod( SemiSimplicityBasis, "for a record and a subgroup chain",
               [ IsRecord, IsSubgroupChain ], function( re, chain )
  if Length( RepresentingMatrices( re.module ) ) = 0  then
    Error("Module has no representing matrices");
  fi;
  if not IsBound( re.basis ) then
    Error("No basis found. Chop with option compbasis:=true");
  fi;
  CH0P_SemiSimplicityBasis( re, chain );
end );

InstallMethod( SemiSimplicityBasis, "usage method",
  [],
  function( )
    Print( "Usage: SemiSimplicityBasis( <choprec>, <subgrpchain> ) where\n" );
    Print( "  <choprec> is an output record of Chop\n");
    Print( "  <subgrpchain> is a subgroup chain\n");
  end );

InstallMethod( IO_Pickle, "for a CHOP module",
  [ IsFile, IsModule ],
  function( f, m )
    if Length(m!.matrices) = 0 then
        Print("You cannot pickle a module without matrices!\n");
        Print("Your current trouble can be caused because you used the ");
        Print("\"destructive\" option.\n");
        Print("You can restore the representing matrices *now* (see \"m\") ",
              " and do \"return;\"\n");
        Print("or you can leave the current pickling, but then you should ");
        Print("call\n\nIO_ClearPickleCache();   for cleanup!\n\n");
        Error("Found module with no representing matrices!");
    fi;
    return IO_GenericObjectPickler(f,"CMOD",[m!.matrices],m,
        [IsSimpleModule,DegreeOfSplittingField,IdWordInfo,SpinUpScript],
        [IsInStandardBasis],
        ["wg","theta","thetaword","thetacharpoly",
         "thetagoodfactor","N","nullvector"]);
  end );

IO_Unpicklers.CMOD :=
  function( f )
    local gens,m;
    gens := IO_Unpickle(f);
    if gens = IO_Error then return IO_Error; fi;
    m := Module(gens);
    m := IO_GenericObjectUnpickler(f,m,
        [IsSimpleModule,DegreeOfSplittingField,IdWordInfo,SpinUpScript],
        [IsInStandardBasis]);
    if m <> IO_Error then
        if Dimension(m) = 1 and ForAll(gens,IsOne) then
            SetFilterObj(m,IsTrivialModule);
        fi;
    fi;
    return m;
  end;

InstallMethod( IsotypicComponentSocle, 
  "for a simple module in standard basis form and another module",
  [ IsModule and IsSimpleModule and IsInStandardBasis, IsModule ],
  function( s, m )
    # Finds all homomorphisms of s into m, returns a record with
    # components "basis" containing a basis of the isotypic component
    # and "cfposs" containing a list of ranges of indices of basis
    # vectors corresponding to the composition factors isomorphic to S
    # in the socle of m.
    local K,a,aa,basis,cfposs,d,deg,i,iw,j,k,l,mgens,ns,ns2,o,seb,sel,sgens,
          sp,v,x;
    d := Dimension(s);
    sgens := RepresentingMatrices(s);
    deg := DegreeOfSplittingField(s);
    mgens := RepresentingMatrices(m);
    if d > Dimension( m ) then
      return rec( basis := ZeroMatrix( 0, Dimension( m ), mgens[1] ),
                  cfposs := [] );
    fi;
    if d = 1 then     # this we can do immediately
        o := OneMutable(mgens[1]);
        x := mgens[1] - sgens[1][1][1]*o;
        ns := NullspaceMatMutableX(x);
        i := 2;
        while i <= Length(mgens) and Length(ns) > 0 do
            x := mgens[i] - sgens[i][1][1]*o;
            x := ns * x;
            ns2 := NullspaceMatMutableX(x);
            ns := ns2 * ns;
            i := i + 1;
        od;
        return rec( basis := ns, cfposs := List([1..Length(ns)],i->[i]) );
    fi;
    Info(InfoChop,3,"Looking for isotypic component of ",Dimension(s),
         "-dim. module in ",Dimension(m),
         "-dim. module...");
    iw := IdWordInfo(s);
    sp := SpinUpScript(s);
    SetWord(m!.wg,iw.word);
    aa := EvaluateWord(m!.wg);
    a := Value(iw!.goodfactor,aa);
    K := [NullspaceMatMutableX(a)];
    Info(InfoChop,3,"Space of homomorphisms could be ",Length(K[1])/deg,
         "-dimensional.");
    if Length(K[1]) = 0 then
        Info(InfoChop,3,"Space of homomorphisms is 0-dimensional.");
        return rec( basis := K[1]{[]}, cfposs := [] );
    fi;
    k := 1;   # counts steps in spinup script
    for i in [1..d] do
        for j in [1..Length(mgens)] do
            if k <= Length(sp.v) and i = sp.v[k] and j = sp.g[k] then
                Add(K,K[i]*mgens[j]);
                k := k + 1;
            else
                x := K[i] * mgens[j];
                for l in [1..Length(K)] do
                    if not(IsZero(sgens[j][i][l])) then
                        x := x - K[l] * sgens[j][i][l];
                    fi;
                od;
                if not(IsZero(x)) then
                    ns := NullspaceMatMutableX(x);
                    if Length(ns) = 0 then
                        Info(InfoChop,3,"Space of homomorphisms is ",
                             "0-dimensional.");
                        return rec( basis := K[1]{[]}, cfposs := [] );
                    fi;
                    Info(InfoChop,3,"Space of homomorphisms could be ",
                         Length(ns)/deg,"-dimensional.");
                    for l in [1..Length(K)] do
                        K[l] := ns * K[l]; 
                    od;
                fi;
            fi;
        od;
    od;
    # If the module is not absolutely irreducible, we have to find a
    # basis of the row space of K[1] over the splitting field:
    if deg > 1 then
        if Length(K[1]) mod deg <> 0 then
            Error("This cannot be, please tell Max!");
            return fail;
        fi;
        seb := EmptySemiEchelonBasis(K[1]);
        CleanRow(seb,ShallowCopy(K[1][1]),true,fail);
        v := K[1][1];
        for i in [2..deg] do
            v := v * aa;
            CleanRow(seb,ShallowCopy(v),true,fail);
        od;
        sel := [1];
        j := 2;
        while Length(sel) < Length(K[1])/deg do
            v := K[1][j];
            if CleanRow(seb,ShallowCopy(v),true,fail) = true then
                j := j + 1;
            else
                for i in [2..deg] do
                    v := v * aa;
                    CleanRow(seb,ShallowCopy(v),true,fail);
                od;
                Add(sel,j);
                j := j + 1;
            fi;
        od;
    else
        sel := [1..Length(K[1])];
    fi;

    # Now put together the basis:
    basis := K[1]{[]};
    cfposs := [];
    for j in sel do
        for i in [1..d] do
            Add(basis,K[i][j]);
        od;
        Add(cfposs,[(j-1)*d+1..j*d]);
    od;
    Info(InfoChop,3,"Found isotypic component with ",Length(cfposs),
         " summands of dimension ",d," each.");
    return rec(basis := basis, cfposs := cfposs);
  end);

InstallGlobalFunction( ShiftRange,
  function( r, sh )
    return [r[1]+sh .. r[Length(r)]+sh];
  end );

InstallMethod( SocleOfModule, 
  "for a module and a database of simple modules in standard basis",
  [ IsModule, IsList ],
  function( m, db )
    local basis,cfposs,i,isotypes,l,pos,r;
    Info(InfoChop,2,"Computing socle of ",Dimension(m),
         "-dimensional module...");
    l := List(db,s->IsotypicComponentSocle(s,m));
    basis := Concatenation(List(l,x->x.basis));
    cfposs := [];
    isotypes := [];
    pos := 0;
    for i in [1..Length(l)] do
        for r in l[i].cfposs do
            Add(cfposs,ShiftRange(r,pos));
            Add(isotypes,i);
        od;
        pos := pos + Length(l[i].basis);
    od;
    Info(InfoChop,2,"Found socle with dimension ",
         Sum(List(cfposs,Length)),".");
    return rec( issoclerecord := true, db := db, module := m,
                basis := basis, ibasis := basis^-1,  ## FIXME basis muss
                                                 ## nicht invertbar sein
                cfposs := cfposs, isotypes := isotypes );
  end );
        
InstallMethod( SocleSeries,
  "for a module and a database of simple modules in standard basis",
  [ IsModule, IsList ],
  function( m, db )
    local basis,cfposs,curgens,d,di,diffpivs,dim,isotypes,mm,newbasis,seb,soc;
    dim := Dimension(m);
    soc := SocleOfModule(m,db);
    basis := soc.basis;
    cfposs := [soc.cfposs];
    diffpivs := [1..dim];
    seb := SemiEchelonBasisMutable(basis);
    SubtractSet(diffpivs,seb!.pivots);
    curgens := RepresentingMatrices(m);
    isotypes := [soc.isotypes];
    while Length(basis) < dim do
        curgens := CH0P_ActionOnQuotient(curgens,seb);
        mm := Module(curgens);
        di := Dimension(mm);
        Info(InfoChop,2,"Remaining dimension: ",di);
        soc := SocleOfModule(mm,db);
        d := Length(soc.basis);
        if d = 0 then
            Error("Found 0-dimensional socle, your database is not complete!");
            return fail;
        fi;
        newbasis := ZeroMatrix( Length(soc.basis), dim, basis );
        CopySubMatrix( soc.basis, newbasis, [1..d], [1..d],
                       [1..di],diffpivs );
        Add(cfposs,List(soc.cfposs,r->ShiftRange(r,Length(basis))));
        Append(basis,newbasis);
        Add(isotypes,soc.isotypes);
        seb := SemiEchelonBasisMutable(soc.basis);
        SubtractSet(diffpivs,diffpivs{seb!.pivots});
    od;
    return rec( issoclerecord := true, db := db, module := m,
                basis := basis, ibasis := basis^-1,
                cfposs := cfposs, isotypes := isotypes);
  end );

InstallMethod( Display, "for a socle record",
  [ IsRecord ],
  function( r )
    local i,j,m;
    if not(IsBound(r.issoclerecord)) then TryNextMethod(); fi;
    Print("socle record for ");
    ViewObj(r.module);
    Print("\n\ndb:");
    i := 1;
    while i <= Length(r.db) do
        Print("\n#   ");
        m := Minimum(i+11,Length(r.db));
        for j in [i..m] do Print(String(j,6)); od;
        Print("\ndim ");
        for j in [i..m] do Print(String(Dimension(r.db[j]),6)); od;
        Print("\nsplt");
        for j in [i..m] do Print(String(DegreeOfSplittingField(r.db[j]),6)); od;
        Print("\n");
        i := i + 12;
    od;
    Print("\nsocle series:\n");
    for i in [Length(r.isotypes),Length(r.isotypes)-1..1] do
        Print("layer ",i,": ");
        for j in [1..Length(r.isotypes[i])] do
            Print(Dimension(r.db[r.isotypes[i][j]]),":#",r.isotypes[i][j]," ");
        od;
        Print("\n");
    od;
  end );
  
InstallMethod( DualModule, "for a module",
  [ IsModule ],
  function( m )
    local mt,x;
    mt := Module( List(RepresentingMatrices(m),TransposedMat) );
    if HasIsSimpleModule(m) and IsSimpleModule(m) then
        # We are in stage (2), so we have to come up with theta and friends:
        mt!.theta := TransposedMat(m!.theta);
        mt!.thetaword := TransposedWord(m!.thetaword);
        mt!.thetacharpoly := m!.thetacharpoly;
        mt!.thetagoodfactor := m!.thetagoodfactor;
        x := Value(m!.thetagoodfactor,m!.theta);
        mt!.N := SemiEchelonBasisNullspace(x);
        SetIsSimpleModule(mt,true);
    fi;
    return mt;
  end );

InstallMethod( DualModule, 
  "for a simple module with known splitting field",
  [ IsModule and IsSimpleModule and HasDegreeOfSplittingField ],
  function( m )
    local mt;
    mt := Module( List(RepresentingMatrices(m),TransposedMat) );
    SetIsSimpleModule(mt,true);
    SetDegreeOfSplittingField(mt,DegreeOfSplittingField(m));
    return mt;
  end );

InstallMethod( DualModule,
  "for a simple module with known splitting field and IdWordInfo",
  [ IsModule and IsSimpleModule and HasDegreeOfSplittingField and
    HasIdWordInfo ],
  function( m )
    local a,aa,iw,iwt,mt,ns;
    mt := Module( List(RepresentingMatrices(m),TransposedMat) );
    SetIsSimpleModule(mt,true);
    SetDegreeOfSplittingField(mt,DegreeOfSplittingField(m));
    # We need to transpose the IdWord:
    iw := IdWordInfo(m);
    iwt := rec( word := TransposedWord(iw.word), charpoly := iw.charpoly,
                goodfactor := iw.goodfactor );
    SetIdWordInfo(mt,iwt);
    # we have to bite the bullet and compute a null vector!
    if Dimension(mt) = 1 then
        mt!.nullvector := One(mt!.matrices[1])[1];
        SetSpinUpScript(mt,rec(v := [], g := [] ));
    else
        SetWord(mt!.wg,iwt.word);
        a := EvaluateWord(mt!.wg);
        aa := Value(iwt.goodfactor,a);
        ns := NullspaceMatMutable(aa);
        mt!.nullvector := ns[1];
        SetSpinUpScript(mt,
                        CH0P_SpinUpScript(mt!.matrices,Dimension(mt),ns[1]));
    fi;
    return mt;
  end );
    

InstallMethod( RadicalOfModule,
  "for a module and a database of simple modules in standard basis",
  [ IsModule, IsList ],
  function( m, db )
    local dbt,dim,mm,mt,rad,seb,soc;
    mt := Module(List(RepresentingMatrices(m),TransposedMat));
    # Now compute 
    dbt := List(db,DualModule);
    for mm in dbt do
        TransformToStandardBasis(mm);
    od;
    soc := SocleOfModule(mt,dbt);
    dim := Length(soc.basis);
    seb := SemiEchelonBasisMutable(soc.basis);
    CH0P_ExtendBasis(seb);
    Append(soc!.basis,seb!.vectors{[Length(soc.basis)+1..Dimension(mt)]});
    soc.ibasis := soc.basis^-1;
    rad := rec( isradicalrecord := true, module := m, db := db,
                basis := TransposedMat(soc.ibasis),
                ibasis := TransposedMat(soc.basis),
                isotypes := ShallowCopy(soc.isotypes),
                cfposs := ShallowCopy(soc.cfposs), 
                raddim := Dimension(mt)-dim,
                radcodim := dim );
    return rad;
  end ); 

InstallMethod( RadicalSeries,
  "for a module and a database of simple modules in standard basis",
  [ IsModule, IsList ],
  function( m, db )
    local dbt,mm,mt,s,tmp;
    mt := Module(List(RepresentingMatrices(m),TransposedMat));
    # Now compute 
    dbt := List(db,DualModule);
    for mm in dbt do
        TransformToStandardBasis(mm);
    od;
    s := SocleSeries(mt,dbt);
    Unbind(s.issoclerecord);
    s.isradicalrecord := true;
    s.module := m;
    s.db := db;
    tmp := TransposedMat(s.basis);
    s.basis := TransposedMat(s.ibasis);
    s.ibasis := tmp;
    s.isotypes := ShallowCopy(s.isotypes);
    s.cfposs := ShallowCopy(s.cfposs);
    return s;
  end );

InstallMethod( Display, "for a radical record",
  [ IsRecord ],
  function( r )
    local i,j,m;
    if not(IsBound(r.isradicalrecord)) then TryNextMethod(); fi;
    Print("radical record for ");
    ViewObj(r.module);
    Print("\n\ndb:");
    i := 1;
    while i <= Length(r.db) do
        Print("\n#   ");
        m := Minimum(i+11,Length(r.db));
        for j in [i..m] do Print(String(j,6)); od;
        Print("\ndim ");
        for j in [i..m] do Print(String(Dimension(r.db[j]),6)); od;
        Print("\nsplt");
        for j in [i..m] do Print(String(DegreeOfSplittingField(r.db[j]),6)); od;
        Print("\n");
        i := i + 12;
    od;
    Print("\nradical series:\n");
    for i in [1..Length(r.isotypes)] do
        Print("layer ",i,": ");
        for j in [1..Length(r.isotypes[i])] do
            Print(Dimension(r.db[r.isotypes[i][j]]),":#",r.isotypes[i][j]," ");
        od;
        Print("\n");
    od;
  end );
  
InstallMethod( InvariantBilinearFormOfModule, 
  "for a module not known to be simple", 
  [ IsModule ],
  function( m )
    local s;
    s := ProperSubmodule(m);
    if s <> fail then
        Error("Cannot determine invariant bilinear form for non-simple module");
        return fail;
    fi;
    return InvariantBilinearFormOfModule(m);    # try again
  end );

InstallMethod( InvariantBilinearFormOfModule, "for a simple module",
  [ IsModule and IsSimpleModule ],
  function( m )
    local iw;
    if not( HasIdWordInfo(m) ) then
        Info(InfoChop,1,"Looking for IdWordInfo...");
        iw := IdWordInfo(m);
    fi;
    Error("to be continued");
  end );

ComputeHomsCyclic := function( M, N, v, poss, rels )
  # M and N are A-modules. This function computes a basis of the space 
  # of homomorphisms from vA \subseteq M to N that map v into the row
  # space of the matrix poss. rels is a list of integer lists
  # containing words in the generators (in the group sense) that
  # are equal to 1. This is only used for optimization.
  # The result is a record
  # with the following components:
  #   cycbasis : the standard basis of vA
  #   cycseb   : corresponding semi echelon basis
  #   cycbc    : cycbc * cycbasis = cycseb
  #   homs     : list of matrices of size Length(cycbasis) x Dimension(N)
  #   spinups  : spinup script for vA
  local K,Mgens,Ngens,a,cycbasis,cycbc,cycseb,d,dec,homs,i,j,k,l,m,mo,ns,
        o,r,raus,s,skip,spinups,w,ww;
  
  Mgens := RepresentingMatrices(M);
  Ngens := RepresentingMatrices(N);
  
  cycbasis := Matrix([],RowLength(Mgens[1]),Mgens[1]);
  cycseb := EmptySemiEchelonBasis(Mgens[1]);
  cycbc := Matrix([],RowLength(Mgens[1]),Mgens[1]);
  dec := ZeroMutable(v);
  CleanRow(cycseb,ShallowCopy(v),true,dec);
  Add(cycbasis,ShallowCopy(v));
  Add(cycbc,ShallowCopy(dec));
  cycbc[1][1] := cycbc[1][1]^-1;
  
  spinups := rec( v := [], g := [] );

  K := [poss];
  i := 1;
  d := 1;
  o := One(BaseDomain(poss));
  mo := -o;
  while i <= d do
      for j in [1..Length(Mgens)] do
          # Check relations whether we can skip something:
          skip := false;
          for r in rels do
              l := Length(r);
              if j = r[l] then
                  a := i;
                  raus := false;
                  l := l - 1;
                  while a > 1 and l > 0 and not(raus) do
                      if spinups.g[a-1] <> r[l] then 
                          raus := true;
                      else
                          l := l - 1;
                          a := spinups.v[a-1];
                      fi;
                  od;
                  if l = 0 then 
                      skip := true; 
                      Info(InfoChop,3,"Used relation ",r);
                      break;
                  fi;
              fi;
          od;
          if not(skip) then 
              w := cycbasis[i] * Mgens[j];
              if CleanRow( cycseb, ShallowCopy(w), true, dec ) then
                  # We got a relation: w = dec * cycseb, however, we want
                  # w = a * cycbasis, thus a = dec * cycbc
                  a := dec{[1..d]} * cycbc;
                  ww := K[i] * Ngens[j];
                  for k in [1..d] do
                      s := -a[k];
                      if not(IsZero(s)) then AddMatrix(ww,K[k],s); fi;
                  od;
                  if not(IsZero(ww)) then
                      ns := NullspaceMatMutableX(ww);
                      if Length(ns) = 0 then
                          Info(InfoChop,3,"Space of homomorphisms is ",
                               "0-dimensional.");
                          return rec( tobedone := true );
                      fi;
                      Info(InfoChop,3,"Space of homomorphisms could be ",
                           Length(ns),"-dimensional.");
                      for k in [1..Length(K)] do
                          K[k] := ns * K[k];
                      od;
                  else
                      Info(InfoChop,3,"Found relation fulfilled everywhere!");
                  fi;
              else    # a new dimension
                  Add(cycbasis,w);
                  ww := dec{[1..d]} * cycbc;  # this creates a new vector
                  MultRowVector(ww,mo);
                  ww[d+1] := o;
                  MultRowVector(ww,dec[d+1]^-1);
                  Add(cycbc,ww);
                  Add(K,K[i]*Ngens[j]);
                  spinups.v[d] := i;
                  spinups.g[d] := j;
                  d := d + 1;
                  if InfoLevel(InfoChop) >= 3 and d mod 100 = 0 then
                      Info(InfoChop,3,"Have dimension ",d);
                  fi;
              fi;
          fi;
      od;
      i := i + 1;
  od;

  # Now K[1] contains a basis of the space of possible images for v
  # under a homomorphism from vA to N.
  homs := [];
  for i in [1..Length(K[1])] do
      m := Matrix([],RowLength(K[1]),K[1]);
      for j in [1..Length(K)] do
          Add(m,K[j][i]);
      od;
      Add(homs,m);
  od;
  return rec( cycbasis := cycbasis, cycseb := cycseb, 
              cycbc := ExtractSubMatrix( cycbc, [1..Length(cycbc)],
                                                [1..Length(cycbc)] ),
              homs := homs );
end;

InstallMethod( PeakwordInfo, 
  "for a simple module in standard basis and a db of simples in std basis",
  [ IsModule and IsInStandardBasis, IsList ],
  function( m, db )
    local e, savedwordm, algel, cpfacs, facsdege, factoroptions, x, N,
          pw, m2, savedword, algel2, x2, N2, peakword, fac, i, k;
  # finds a peakword for the simple module m with respect to the database db
  # in other words, finds an idword with stable kernel on m, which has nullity
  # 0 on all other simples in the database
    e := DegreeOfSplittingField( m );
    savedwordm := CurrentWord(m!.wg);
    while true do
      NextWord( m!.wg );
      algel := EvaluateWord( m!.wg );
      cpfacs := CharacteristicPolynomialOfMatrix( algel );
      facsdege := [];
      for fac in cpfacs.factors do
        Append(facsdege, 
        Factors( PolynomialRing(BaseField( algel ) ), 
                 fac : factoroptions := rec( onlydegs := [e] ) ));
      od;
      if Length(facsdege) = 0 then
        continue;
      fi;
      facsdege := Collected(facsdege);
      for i in [ 1..Length( facsdege ) ] do
        x := Value( facsdege[ i ][ 1 ], algel );
        N := NullspaceMatMutable( x );
        if Length( N ) = e then
        Info(InfoChop,3,"Found IdWord");
          # Found idword
          # check if kernel is stable
          if Length( NullspaceMatMutable( x * x ) ) = e then
            Info(InfoChop,3,"Its kernel is stable");
            # kernel is stable
            # check on all other simples
            for k in [ 1 .. Length( db ) ] do
              pw := true;
              if m = db[ k ] then
                continue;
              else
                m2 := db[ k ];
                savedword:=CurrentWord( m2!.wg ); 
                SetWord( m2!.wg, m!.wg!.word ); 
                algel2 := EvaluateWord( m2!.wg );
                SetWord( m2!.wg, savedword );
                x2 := Value( facsdege[ i ][ 1 ], algel2 );
                N2 := NullspaceMatMutableX( x2 );
                if Length( N2 ) <> 0 then
                  Info(InfoChop,3,"No regular action on ",k);
                  # no peakword for m
                  pw := false;
                  break;
                fi;
              fi;
            od;
            
            if pw = false then
              break; # take next poly
            else
              # peakword found
              Info(InfoChop,3,"Found peakword");
              peakword := CurrentWord( m!.wg );
              # reset idword
              SetWord( m!.wg, savedwordm );
              return rec( word:=peakword,
                          goodfactor := facsdege[i][1] );
            fi;
          fi;
        fi;
      od;
    od;
  end );


InstallMethod( KernelOfPeakword,
  "for a module and a peakword",
  [ IsModule, IsRecord ],
  function( m, pw )
    local savedword, algel, x, xx, nsp, nsp2;
    savedword := CurrentWord( m!.wg );
    SetWord( m!.wg, pw.word );
    algel := EvaluateWord( m!.wg );
    SetWord( m!.wg, savedword );
    x := Value( pw.goodfactor, algel );
    #xx := ShallowCopy( x );
    repeat
      #nsp := NullspaceMatMutable( xx );
      #xx := xx * x;
      #nsp2 := NullspaceMatMutable( xx );
      nsp := NullspaceMatMutable( x );
      x := x * x; #Bauerntrick
      nsp2 := NullspaceMatMutable( x );
    until Length( nsp2 ) = Length( nsp );
    return nsp;
  end );

InstallGlobalFunction( CH0P_SpinAndScript, function( gens, basis, dim, seed )
  local old, report, counter, spinupscript, im, i;
  # INPUT
  # gens  : matrices
  # basis : record with fields
  #         pivots  : integer list giving pivot columns
  #         vectors : basis vectors in semi-echelon form
  # dim   : the dimension of the subspace to spin up, if unkown set to
  #         maximal dimension
  # seed  : seed vector of same length as rows of gens
  # OUTPUT
  # returns spinupscript.
  # changes basis to basis of smallest subspace
  # invariant under gens containing 'basis' and 'seed'
  # NOTES
  # 'basis' is assumed to span an invariant space
  # destructive in arguments 'basis' and 'seeds'
  old := Length( basis!.vectors );
  report := Maximum(100,QuoInt(dim,20));
  CleanRow( basis, seed, true, fail );
  counter := Length( basis!.vectors );
  if old = counter then 
    return true; #quick exit
  fi;
  spinupscript:=rec( v:=[], g:=[] );
  while counter <= Length( basis!.vectors ) and 
        Length( basis!.vectors ) < dim do
    for i in [1..Length(gens)] do
      im := basis!.vectors[ counter ] * gens[ i ];
      if CleanRow( basis, im, true, fail ) = false then
        Add( spinupscript.v, counter );
        Add( spinupscript.g, i );
      fi;
    od;
    counter := counter + 1;
    if InfoLevel(InfoChop) > 2 and counter mod report = 1 then
       Info(InfoChop,3,"Spinning pos=",counter," have=",
         Length(basis!.vectors), " maxdim=",dim);
    fi;
  od;
  Info(InfoChop,3,"Spun up to dimension ",Length(basis!.vectors));
  return spinupscript;
end );

InstallMethod( Homomorphisms, 
  "for a chop record and another module",
# M und N muessen als Darstellungen derselben Algebrenelemente als Erz haben
  [ IsRecord, IsModule ],
  function( M_ch, N )
    local dimN, M, dimM, Mgens, Ngens, K, spb, decs, spbptr, layerptr, 
          basch, literalbas, dec, curdim, A, rows, initheight, 0mat, im, 
          litdec, mat, ns, homs, iliteralbas, h, row, cf, mountain, v, 
          k, i, l;
    dimN := Dimension(N);
    M := M_ch.module;
    dimM := Dimension(M);
    if not HasKernelsOfPeakwords( M ) 
      or not HasKernelsOfPeakwords( N ) then
      Error( "M and N need to have KernelsOfPeakwords." );
      return;
    fi;
    Mgens := RepresentingMatrices(M);
    Ngens := RepresentingMatrices(N);
    # do the grand loop over composition factors
    K := [ ]; # store images here
    spb := EmptySemiEchelonBasis( Mgens[ 1 ] ); # spinning basis
    decs := []; # store decompositions of spb here
    spbptr := 0; # points to current vector in spinning basis
    layerptr := 0; # points to current layer
    basch := ZeroMatrix( dimM, dimM, Mgens[1] ); # store basischange from
    literalbas := ZeroMutable( basch ); # store literal basis
    # spinning basis to literal basis
    for cf in [1..Length(M_ch.db)] do
      Info(InfoChop,3,"Looking for mountain of composition",
        " factor ", cf,"...");
      # loop over peakword kernel basis
      for v in KernelsOfPeakwords( M )[ cf ] do
        dec := ZeroMutable(v);
        if not CleanRow( spb, ShallowCopy( v ), true, dec ) then
          # v is not yet in spinning basis
          # organize basis change mat
          curdim := Length( spb!.vectors );
          basch[ curdim ][ curdim ] := dec[ curdim ]^-1;
          if curdim > 1 then
            A := ExtractSubMatrix( basch, [1..curdim-1],
                 [1..curdim-1] );
            CopySubVector( -dec[ curdim ]^-1 * 
                  dec{[1..curdim-1]} * A, 
                  basch[curdim], [1..curdim-1], [1..curdim-1] );
          fi;
          literalbas[ curdim ] := v;

          spbptr := Length( spb!.vectors ); #point to newly appended vector
          # do the trivial blow up. ie assume direct sum of homs
          if IsBound( K[1] ) then
            rows := Length( K[1] ) + Length( KernelsOfPeakwords(N)[cf] );
            initheight := Length( K[1] );
          else;
            rows := Length( KernelsOfPeakwords(N)[cf] );
          fi;
          for k in [1..Length(K)] do
            0mat := ZeroMatrix( rows, dimN, KernelsOfPeakwords( N )[ cf ] );
            0mat{ [1..Length(K[k])] } := K[k];
            K[k]:=0mat;
          od;
          if not IsBound( K[1] ) then
            Add( K, KernelsOfPeakwords( N )[ cf ] );
          else
            0mat := ZeroMatrix( rows, dimN, KernelsOfPeakwords( N )[ cf ] );
            0mat{[initheight+1..rows]} := KernelsOfPeakwords(N)[cf];
            Add( K, 0mat );
          fi;
        fi;
        while spbptr <= Length( spb!.vectors ) do
          for i in [ 1 .. Length( Mgens ) ] do
            im := literalbas[ spbptr ] * Mgens[ i ];
            dec := ZeroMutable( v ); # store decomposition here
            if CleanRow( spb, ShallowCopy(im), true, dec ) = false then
              curdim := Length(spb!.vectors);
              basch[ curdim ][ curdim ] := dec[ curdim ]^-1;
              A := ExtractSubMatrix( basch, [1..curdim-1], [1..curdim-1] );
              CopySubVector( -dec[ curdim ]^-1 * 
                    dec{[1..curdim-1]} * A, 
                    basch[ curdim ], [1..curdim-1], [1..curdim-1] );
              literalbas[ curdim ] := im;
              # we get no relation, append space
              Add( K, K[ spbptr ] * Ngens[ i ] );
            else
              # we get a relation
              # transform to literal basis
              litdec := dec * basch;
              # K[l]'s and spb vectors correspond

              # loop over layers
              mat := K[ spbptr ] * Ngens[ i ];
              for l in [ 1 .. Length( K ) ] do
                if not IsZero( litdec[ l ] ) then
                    mat := mat - litdec[ l ] * K[ l ];
                fi;
              od;
              # mat now stores lk's
              # init large matrix
              if not IsZero( mat ) then
                ns := NullspaceMatMutableX( mat );
                if Length( ns ) = 0 then
                  #Error("Das Werk ist kaputt!");
                  #Print("#I Warning: Das Werk ist kaputt.\n");
                fi;
                for l in [1..Length(K)] do
                    K[ l ] := ns * K[ l ];
                od;
              fi;
            fi;
          od; # next generator
          spbptr := spbptr + 1; # update spinning basis pointer
        od; # next vector in spb
      od; # next vector in peakword kernel
    od; # next CF
    # now K should store all homomorphisms line by line
    homs := [];;
    iliteralbas:=literalbas^-1;
    for k in [1..Length( K[1] )] do
      h := ZeroMatrix( dimM, dimN, Mgens[ 1 ] );
      row := 1;
      for l in [1..Length(K)] do
        h[row] := K[l][k];
        row := row + 1;
      od;
      Add( homs, iliteralbas * h );
    od;
    return homs;
  end );

InstallGlobalFunction( CH0P_Furl, 
#
# converts a cvec of length m*n into a
# CMat with m rows and n columns.
# Klaus
#
  function( v, m , n )
    local  V, i;
    if m * n <> Length( v ) then
      Error("Dimensions do not match.");
    fi;

    V := CVEC_ZeroMat( m, CVecClass( v, n ) );
    for i  in [ 1 .. m ]  do
      V[ i ] := v{ [ (i - 1) * n + 1 .. i * n ] };
    od;
    return V;
  end );

InstallGlobalFunction( CH0P_Unfurl,
#
# flattens a CMat with m rows and n columns
# into a cvec of length m*n. 
# Klaus
  function( V )
    local  m, n, v, i;
    m := Length( V );
    n := Length( V[1] );
    v := ZeroVector( m * n, V[1] );
    for i  in [ 1 .. m ]  do
      CopySubVector( V[i], v, [ 1 .. n ], [ (i - 1) * n + 1 .. i * n ] );
    od;
    return v;
  end );

InstallGlobalFunction( CH0P_TensorMultiply,
# given a cvec of length m*n, converts it
# into a CMat with m rows and n columns.
# Then multiplies this matrix on the left
# by the matrix Atr and on the right by the
# matrix B. Finally converts the resulting
# matrix back into a cvec of length m*n.
# Klaus.
  function( v, Atr, B )
    return CH0P_Unfurl( Atr * 
           CH0P_Furl( v, Length( Atr[1] ), Length( B ) ) * B );
  end );

InstallGlobalFunction( CH0P_TensorSpin,
#
# Similar to CH0P_Spin.
  # INPUT
  # Mtr,N  : list of matrices
  # B : record with fields
  #         pivots  : integer list giving pivot columns
  #         vectors : basis vectors in semi-echelon form
  # limit   : the dimension of the subspace to spin up, if unkown set to
  #         maximal dimension
  # v  : seed vector of length nr rows of the matrices in
  # Mtr times 
  # nr. of rows of the matrices in N.
  # OUTPUT
  # returns nothing.
  # changes B to basis of smallest subspace
  # invariant under the tensor (Kronecker product)
  # action of # Mtr and N containing 'B' and 'seed'
  # NOTES
  # 'B' is assumed to span an invariant space
  # destructive in arguments 'B' and 'v'
  # this is mainly used in the situation where
  # Mtr and N are matrices of fixed  group
  #  generators  in two representations a group G.
  # More precisely Mtr actually are the transposed matrices
  # for the representation.
  # We use  the fact that if F^m and F^n are the underlying
  # row spaces of the representations then
  # the m by n matrices over F as a module
  # for G with action M^t(g)*mat*M(g)  and
  # F ^mn (= F^m tensor F^n) as  a module for  G with the
  # Kronecker matrix product action of M and N
  # are isomorphic as G-modules. The isomorphisms
  # are given by CH0P_Furl and CH0P_Unfurl, see above.
  # Klaus.
#
  function( v, Mtr, N, B, limit )
    local report, old, counter, im, i;
    report := 100;
    if limit <= Length( B!.vectors )  then
        limit := Length( Mtr[1] ) * Length( N[1] );
    fi;
    old := Length( B!.vectors );
    CleanRow( B, v, true, fail );
    counter := Length( B!.vectors );
    if old = counter  then
        return true;
    fi;
    while counter <= Length( B!.vectors ) and Length( B!.vectors ) < limit  do
        for i  in [ 1 .. Length( Mtr ) ]  do
            im := CH0P_TensorMultiply( B!.vectors[counter], Mtr[i], N[i] );
            CleanRow( B, im, true, fail );
        od;
        counter := counter + 1;
        if InfoLevel(InfoChop) > 2 and counter mod report = 1 then
            Info(InfoChop,3,"Spinning pos=",counter," have=",
                 Length(B!.vectors)," maxdim=",limit);
        fi;
    od;
    return true;
  end );

InstallGlobalFunction( CH0P_TensorActionOnSubspace,
  function( lgenstr, rgens, basis )
    local dim, zerov, imgens, v, w, i, j;
    # INPUT
    # lgenstr  : List of transposed matrices of left tensor factor
    # rgens  : List of matrices of right tensor factor
    # basis : basis of invariant subspace ie record with fields
    #         pivots   : integer list of pivot columns
    #         vectors : matrix of basis in semi-echelon form
    # OUTPUT
    # List of matrices representing the action of the
    # the generators of the group on
    # the submodule with 'basis' as a basis in
    # the Kroneckerproduct action.
    #
    dim:=Length(basis!.vectors);
    zerov := ZeroVector( dim, basis!.vectors[1] ); #prepare vector type
    imgens := []; # stores result
    for i in [ 1 .. Length( rgens ) ] do
      imgens[ i ] := ZeroMatrix(dim, dim, rgens[1] );
      for j in [1..dim] do
        v := basis!.vectors[ j ];
        w := ShallowCopy( zerov );
        CleanRow( basis, CH0P_Unfurl (lgenstr[ i ] * 
                  CH0P_Furl( v,Length(lgenstr[1]),
                  Length(rgens[1])) * rgens[ i ] ), false, w );
        CopySubVector( w, imgens[ i ][ j ], [1..dim], [1..dim] );
      od;
    od;
    return imgens;
  end );





