#############################################################################
##
##  chop.gd              chop package                        Max Neunhoeffer
##                                                              Felix Noeske
##
##  Copyright 2005 Lehrstuhl D f�r Mathematik, RWTH Aachen
##
##  Declaration stuff for chop
##
#############################################################################

#############################################################################
# For information:
#############################################################################

DeclareInfoClass( "InfoChop" );

#############################################################################
# The typing stuff for module objects:
#############################################################################

DeclareCategory( "IsModule", IsObject );
BindGlobal( "ModulesFamily", NewFamily( "ModulesFamily", IsModule ) );
DeclareGlobalVariable("ModuleType");

#############################################################################
# Attributes and properties and similar things for modules:
#############################################################################

# <#GAPDoc Label="ModuleStatesDescription">
# A module object can be in 5 different states. A reducible module will
# always stay in the first one, a simple module will usually run through
# all 5 different stages in the following order:
#  <Enum>
#  <Item>A module that either is reducible or does not yet know that it is 
#      simple: not(HasIsSimpleModule) or not(IsSimpleModule)</Item>
#  <Item>A module that already knows that it is simple but does not yet know
#      the degree of its splitting field:
#      (HasIsSimpleModule and IsSimpleModule and not(HasDegreeOfSplittingField)</Item>
#  <Item>A simple module that knows the degree of its splitting field but does 
#      not yet have an IdWord
#      (HasIsSimpleModule and IsSimpleModule and HasDegreeOfSplittingField and
#       not(HasIdWordInfo))</Item>
#  <Item>A simple module that knows the degree of its splitting field and
#      has an IdWord but is not yet in standard base
#      (HasIsSimpleModule and IsSimpleModule and HasDegreeOfSplittingField and
#       HasIdWordInfo and not(IsInStandardBasis))</Item>
#  <Item>A simple module in standard basis
#      (HasIsSimpleModule and IsSimpleModule and HasDegreeOfSplittingField and
#       HasIdWordInfo and IsInStandardBasis)</Item>
#  </Enum>
# No other combinations of these filters are possible.
# Sometimes a module object does not go through all these stages during
# its lifetime. This happens if during certain calculations additional
# further information is found. The standard life cycle of a simple module
# is:
#  <List>
#   <Item>Created by "Module"      --> (1)</Item>
#   <Item>ProperSubmodule          --> (2)  (reducible modules stay in (1) forever)</Item>
#   <Item>DegreeOfSplittingField   --> (3)</Item>
#   <Item>IdWordInfo               --> (4)</Item>
#   <Item>TransformToStandardBasis --> (5)</Item>
#  </List>
# For each state, it is well-defined, which components, properties and
# attributes are set or not:

# The following are always present for all modules from the beginning on:<P/>
#
# Components:
#  <List>
#   <Item>"matrices": a list of representing matrices</Item>
#   <Item>"field": the base field</Item>
#   <Item>"wg": a word generator, is deleted to save memory when 
#         the module is found to be reducible when "destructive" is set</Item>
#  </List>
#
# For the standard word generator:
#  <List>
#   <Item>A word is a record of three lists w = rec( mul:=[..],coe:=[..],del:=[..] )</Item>
#   <Item>mul is a list of integer pairs, indicating which els to multiply</Item>
#   <Item>coe is a list of coefficients in the base field (may contain holes)</Item>
#   <Item>del is a list of integer lists, indicating which els to delete</Item>
#  </List>
# Algorithm:
#  <Verb>
#   wordcache := gens
#   l := Length(gens)
#   for i in [1..Length(mul)] do
#     wordcache[l+i] := wordcache[mul[i][1],mul[i][2]]
#     for j in del[i] do 
#       Unbind(wordcache[j])
#   res := Zero
#   for i in [1..Length(coe)] do
#     if IsBound(coe[i]) then
#       res := res + wordcache[i] * coe[i]
#  </Verb>
# <#/GAPDoc>

# Attributes and properties:
DeclareOperation( "RepresentingMatrices", [ IsModule ] );
DeclareAttribute( "Dimension", IsModule );
DeclareAttribute( "MemoryPerRow", IsModule );
DeclareAttribute( "MemoryPerMatrix", IsModule );
DeclareOperation( "Memory", [IsModule] );

# The following properties and attributes determine the stage (1)-(5) from
# above:

DeclareProperty( "IsSimpleModule", IsModule );
DeclareFilter( "IsTrivialModule", IsModule );  # dim 1 and all gens IsOne
DeclareFilter( "IsZeroModule", IsModule ); #cannot be created sanely

DeclareAttribute( "DegreeOfSplittingField", IsModule );

DeclareAttribute( "IdWordInfo", IsModule );
# An IdWordInfo is a record with components: 
#        word         : see documentation above
#        charpoly     : the characteristic polynomial of the algebra element
#        goodfactor   : good factor of charpoly

DeclareFilter( "IsInStandardBasis" );

# <#GAPDoc Label="ModuleStageInfo">
# The following components are present iff the module is in stage (2):
#  <List>
#   <Item>"theta":           an algebra element as a matrix</Item>
#   <Item>"thetaword":       the word to reach theta from the generators</Item>
#   <Item>"thetacharpoly":   the characteristic polynomial of theta</Item>
#   <Item>"thetagoodfactor": a good factor of thetacharpoly
#                      note: is equal to zero if thetacharpoly irred.</Item>
#   <Item>"N":               the nullspace of thetagoodfactor(theta)</Item>
#  </List>
# These components are bound during "ProperSubmodule" and unbound in
# "DegreeOfSplittingField".
# <P/>
# Note that if a module was found to be reducible during a run of
# ProperSubmodule, then the component "thetagoodfactor" is bound
# to the good factor used together with the current word to prove this.
# This is used in one place in CH0P_CHOP!
# <P/>
# The following components are bound iff the module is in stage (4):
# <List>
#  <Item> "nullvector":      a vector in the nullspace N above</Item>
# </List>
# This component is bound during "DegreeOfSplittingField" or "IdWordInfo"
# when an IdWord is found and is unbound during "TransformToStandardBasis".
# <#/GAPDoc>
# At the same time the following attribute is set:


DeclareAttribute( "SpinUpScript", IsModule );

##### experimental peakword implementation

DeclareAttribute( "PeakwordInfo", IsModule);
# An IdWordInfo is a record with components: 
#        word         : see documentation above
#        goodfactor   : good factor of charpoly

DeclareAttribute( "KernelsOfPeakwords", IsModule );
# KernelOfPeakword is the nullspace matrix of the peakword
# List of kernels relative to some fixed database

#############################################################################
# Constructor operations:
#############################################################################

DeclareOperation( "Module", [ IsList, IsRecord ] );
DeclareOperation( "Module", [ IsList ] );

#############################################################################
# Internal functions to work with modules, words, basis, spaces:
#############################################################################

DeclareGlobalVariable( "CH0P_GlobalOpts" );
DeclareGlobalFunction( "CH0P_Spin" );
DeclareGlobalFunction( "CH0P_SpinStandardBasis" );
DeclareGlobalFunction( "CH0P_SpinUpScript" );
DeclareGlobalFunction( "CH0P_SpinWithScript" );
DeclareGlobalFunction( "CH0P_ActionOnSubspace" );
DeclareGlobalFunction( "CH0P_ExtendBasis" );
DeclareGlobalFunction( "CH0P_ActionOnQuotient" );
DeclareGlobalFunction( "CH0P_OrthogonalVector" );
DeclareGlobalFunction( "CH0P_SpinSubspace" );
DeclareGlobalFunction( "CH0P_CopyModule" );
DeclareGlobalFunction( "CH0P_CHOP" );
DeclareGlobalFunction( "CH0P_SemiSimplicityBasis" );


#############################################################################
# The operations the user should use:
#############################################################################


# <#GAPDoc Label="ProperSubmoduleHeader">
#   <ManSection>
#     <Filt Type="Operation" Name="ProperSubmodule" Arg="IsModule, [IsList, IsList]"/>
#  <Returns>
#  fail if the module is irreducible
#  a semi-echelonised basis of a proper submodule otherwise
#  </Returns>
#  <Description>
# Stores (in the module object):
#  in the irreducible case:
#   sets IsSimpleModule to true
#   the current word was able to prove simplicity
#  in the reducible case:
#   sets IsSimpleModule to false
#  </Description>
# </ManSection>
# <#/GAPDoc>
DeclareOperation( "ProperSubmodule", [IsModule] );
DeclareOperation( "ProperSubmodule", [IsModule, IsList, IsList] );


# <#GAPDoc Label="ChopHeader">
#   <ManSection>
#     <Filt Type="Operation" Name="Chop" Arg="[IsModule, IsRecord]"/>
#   <Returns>
#  a record with the following components:
#    <List>
#     <Item>"ischopmodule": true</Item>
#     <Item>"module":    the module object itself</Item>
#     <Item>"db":        a list of simple modules in standard basis</Item>
#     <Item>"mult":      multiplicities of the simples in db in module</Item>
#     <Item>"acs":       describes an ascending composition series, a list with
#                 integers in [1..Length(db)]</Item>
#     <Item>"splittree": a binary tree of words used during the Chop
#                 this can be used as "hints" tree to exactly repeat the same
#                 calculation!</Item>
#     <Item>if compbasis=true:
#    "basis":    a basis adapted to the composition series</Item>
#    </List>
#   </Returns>
#   </ManSection>
# <#/GAPDoc>
DeclareOperation( "Chop", [] );
DeclareOperation( "Chop", [IsModule] );
DeclareOperation( "Chop", [IsModule, IsRecord] );

DeclareOperation( "TransformToStandardBasis", [ ] );
DeclareOperation( "TransformToStandardBasis", [ IsModule and IsSimpleModule ] );

# <#GAPDoc Label="SemiSimplicityBasisHeader">
#   <ManSection>
#     <Filt Type="Operation" Name="SemiSimplicityBasis" Arg="[IsRecord, IsSubgroupChain]"/>
#     <Description>
# Takes a record re coming from Chop as first argument and a subgroup chain.
# Calculates a semisimplicity basis and adjusts re.basis. The inverse
# of re.basis is bound to re.ibasis. Also the
# component gensinssb of re is bound to the list of generators in
# that new basis, that is, in the end we have:
# re.gensinssb = List(re.module!.matrices,x->re.basis*x*re.ibasis)
#     </Description>
#   </ManSection>
# <#/GAPDoc>
DeclareOperation( "SemiSimplicityBasis", [] );
DeclareOperation( "SemiSimplicityBasis", [IsRecord, IsSubgroupChain] );


# <#GAPDoc Label="IsomorphismHeader">
#   <ManSection>
#     <Filt Type="Operation" Name="Isomorphism" Arg="[IsModule, IsModule]"/>
#     <Description>
# The first module  m1 must be a simple module in standard basis.
#  In case that the second module m2  is isomorphic to the first, 
#  returns a matrix b that the representing matrices of m2
#  are equal to the representing matrices of m1.
#     </Description>
#   </ManSection>
# <#/GAPDoc>
DeclareOperation( "Isomorphism", [IsModule, IsModule] );

# <#GAPDoc Label="IsotypicComponentHeader">
#   <ManSection>
#     <Filt Type="Operation" Name="IsotypicComponent" Arg="[IsModule, IsModule]"/>
#     <Description>
# Finds all homomorphisms of the first module s into  the
# second module m.
# returns a record re  with components
# <List>
# <Item>  "basis" containing a basis of the isotypic component  </Item>
# <Item> "cfposs" containing a list of ranges of indices of basis 
# vectors corresponding to the composition factors
# isomorphic to S in the socle of m. </Item>
# </List>
#     </Description>
#   </ManSection>
# <#/GAPDoc>

DeclareOperation( "IsotypicComponentSocle", [ IsModule, IsModule ] );
DeclareGlobalFunction( "ShiftRange" );
# <#GAPDoc Label="SocleOfModuleHeader">
#   <ManSection>
#     <Filt Type="Operation" Name="SocleOfModule" Arg="[IsModule, IsList]"/>
#     <Description>
# for a module m and a (database) list  of simple modules in standard basis
# returns a  socle record re containing
# <List>
# <Item> re.issoclerecord true </Item>
# <Item> re.db the a database</Item>
# <Item> re.module m</Item>
# <Item> re.basis basis of the socle</Item>
# <Item> re.cfposs</Item>
# <Item> re.isotpyes.</Item>
# <Item> re.ibasis the inverse of re.basis, which does not have to exist and is A
# BUG, see source code comment.</Item>
# </List>
# it calls IsotypicComponentSocle.
#     </Description>
#   </ManSection>
# <#/GAPDoc>
DeclareOperation( "SocleOfModule", [ IsModule, IsList ] );

# <#GAPDoc Label="SocleSeriesHeader">
#   <ManSection>
#     <Filt Type="Operation" Name="SocleSeries" Arg="[IsModule, IsList]"/>
#     <Description>
# for a module m and a (database) list  of simple modules in standard basis
# returns a  socle series record re containing
# <List>
# <Item> re.issoclerecord true </Item>
# <Item> re.db the  database</Item>
# <Item> re.module m</Item>
# <Item> re.basis basis adjusted to the socle series</Item>
# <Item> re.cfposs list of lists compare to SocleOfModule.</Item>
# <Item> re.isotpyes. ditto </Item>
# <Item> re.ibasis the inverse of re.basis.</Item>
# </List>
# iterativly calls SocleOfModule.
#     </Description>
#   </ManSection>
# <#/GAPDoc>
DeclareOperation( "SocleSeries", [ IsModule, IsList ] );
# <#GAPDoc Label="RadicalOfModuleHeader">
#   <ManSection>
#     <Filt Type="Operation" Name="RadicalOfModule" Arg="[IsModule, IsList]"/>
#     <Description>
# for a module m and a (database) list  of simple modules in standard basis
# returns a  radical record re containing
# <List>
# <Item> re.isradicalrecord true </Item>
# <Item> re.db the a database</Item>
# <Item> re.module m</Item>
# <Item> re.basis basis of the socle</Item>
# <Item> re.cfposs</Item>
# <Item> re.isotpyes.</Item>
# <Item> re.raddim</Item>
# <Item> re.radcodim</Item>
# <Item> re.ibasis the inverse of re.basis, which does not have to exist and is A
# BUG, see source code comment.</Item>
# </List>
# it calls IsotypicComponentSocle.
#     </Description>
#   </ManSection>
# <#/GAPDoc>

DeclareOperation( "RadicalOfModule", [ IsModule, IsList ] );
# <#GAPDoc Label="RadicalSeriesHeader">
#   <ManSection>
#     <Filt Type="Operation" Name="RadicalSeries" Arg="[IsModule, IsList]"/>
#     <Description>
# for a module m and a (database) list  of simple modules in standard basis
# returns a  radical series record re containing
# <List>
# <Item> re.isradicalrecord true </Item>
# <Item> re.db the  database</Item>
# <Item> re.module m</Item>
# <Item> re.basis basis adjusted to the radical series</Item>
# <Item> re.cfposs list of lists compare to SocleOfModule.</Item>
# <Item> re.isotpyes. ditto </Item>
# <Item> re.ibasis the inverse of re.basis.</Item>
# </List>
# calls SocleSeries.
#     </Description>
#   </ManSection>
# <#/GAPDoc>

DeclareOperation( "RadicalSeries", [ IsModule, IsList ] );

DeclareOperation( "DualModule", [ IsModule ] );

DeclareOperation( "InvariantBilinearFormOfModule", [ IsModule ] );
DeclareOperation( "PrimitiveElementEndo", [ IsModule ] );
DeclareOperation( "PrimitiveRootEndo", [ IsModule ] );

# experimental peakword implementation
DeclareOperation( "PeakwordInfo", [IsModule and IsInStandardBasis, IsList]);
DeclareOperation( "KernelOfPeakword", [ IsModule, IsRecord ] );
DeclareGlobalFunction( "CH0P_SpinAndScript" );
DeclareOperation( "Homomorphisms", [IsRecord, IsModule] );

# Stuff to deal efficiently with tensorproducts
DeclareGlobalFunction( "CH0P_Furl" );
DeclareGlobalFunction( "CH0P_Unfurl" );
DeclareGlobalFunction( "CH0P_TensorMultiply" );
DeclareGlobalFunction( "CH0P_TensorSpin" ); 
DeclareGlobalFunction( "CH0P_TensorActionOnSubspace" );
